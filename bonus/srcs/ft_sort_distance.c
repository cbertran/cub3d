/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sort_distance.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cbertran <cbertran@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/10 16:57:45 by cbertran          #+#    #+#             */
/*   Updated: 2020/05/16 18:01:43 by cbertran         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

static void	fill_save_tab(t_sort_sprite *var, t_sprite *tab, int end_one,
							int start_one)
{
	while (var->i_save <= end_one)
	{
		var->save_tab[var->i_save - start_one] = tab[var->i_save];
		var->i_save++;
	}
}

static void	fusion(t_sprite *tab, int start_one, int end_one, int end_two)
{
	t_sort_sprite		var;

	var.count_one = start_one;
	var.count_two = end_one + 1;
	if (!(var.save_tab = malloc((end_one - start_one + 1) * sizeof(t_sprite))))
		return ;
	var.i = start_one;
	var.i_save = start_one;
	fill_save_tab(&var, tab, end_one, start_one);
	while (var.i <= end_two)
	{
		if (var.count_one == (end_one + 1))
			break ;
		else if (var.count_two == (end_two + 1))
			tab[var.i] = var.save_tab[var.count_one++ - start_one];
		else if (var.save_tab[var.count_one - start_one].distance
					> tab[var.count_two].distance)
			tab[var.i] = var.save_tab[var.count_one++ - start_one];
		else
			tab[var.i] = tab[var.count_two++];
		var.i++;
	}
	free(var.save_tab);
}

static void	launch_sort(t_sprite *tab, int start, int end)
{
	int	middle;

	if (start != end)
	{
		middle = (end + start) / 2;
		launch_sort(tab, start, middle);
		launch_sort(tab, middle + 1, end);
		fusion(tab, start, middle, end);
	}
}

void		ft_sort_sprite(t_sprite *tab, int length)
{
	if (length > 0)
		launch_sort(tab, 0, length - 1);
}

void		calc_distance_and_sort_sprite(t_env_cub3d *env)
{
	int		i;
	double	dx;
	double	dy;

	i = 0;
	while (i < env->nbr_sprite)
	{
		env->sprite[i].id = i;
		dx = env->sprite[i].x - env->player.pos.x;
		dy = env->sprite[i].y - env->player.pos.y;
		env->sprite[i].distance = sqrt(dx * dx + dy * dy);
		env->sprite[i].angle = (atan2(dy, dx)) / DEG_TO_RAD;
		(env->sprite[i].angle < 0) ? (env->sprite[i].angle += 360) : 0;
		i++;
	}
	ft_sort_sprite(env->sprite, env->nbr_sprite);
	env->current_sprite = 0;
}
