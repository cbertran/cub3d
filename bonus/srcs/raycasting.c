/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   raycasting.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cbertran <cbertran@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/31 22:13:27 by cbertran          #+#    #+#             */
/*   Updated: 2020/06/01 14:25:30 by cbertran         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

/*
**	Explanation of order and logical calc
**
**	 +FOV/2   -FOV/2		|			y
**  	\       /		 3  |  2		|
**  	 \     /	   -----|-----		|
**  	  \   /			 4  |  1		|
**			O				|			o ------ x
*/

static void	define_step(t_env_cub3d *env)
{
	if (env->ray.dir_x < 0)
	{
		env->ray.step_x = -1;
		env->ray.side_dist_x = (env->ray.orig_x - env->ray.map_x)
								* env->ray.delta_dist_x;
	}
	else
	{
		env->ray.step_x = 1;
		env->ray.side_dist_x = (env->ray.map_x + 1.0 - env->ray.orig_x)
								* env->ray.delta_dist_x;
	}
	if (env->ray.dir_y < 0)
	{
		env->ray.step_y = -1;
		env->ray.side_dist_y = (env->ray.orig_y - env->ray.map_y)
								* env->ray.delta_dist_y;
	}
	else
	{
		env->ray.step_y = 1;
		env->ray.side_dist_y = (env->ray.map_y + 1.0 - env->ray.orig_y)
								* env->ray.delta_dist_y;
	}
}

static void	found_wall(t_env_cub3d *env)
{
	while (env->ray.hit == 0)
	{
		if (env->ray.side_dist_x < env->ray.side_dist_y)
		{
			env->ray.side_dist_x += env->ray.delta_dist_x;
			env->ray.map_x += env->ray.step_x;
			env->ray.side = 0;
		}
		else
		{
			env->ray.side_dist_y += env->ray.delta_dist_y;
			env->ray.map_y += env->ray.step_y;
			env->ray.side = 1;
		}
		env->ray.map_case = env->map[env->ray.map_x][env->ray.map_y];
		if_detect(env);
	}
	if (!env->ray.hit)
	{
		env->ray.end_x = env->ray.orig_x;
		env->ray.end_y = env->ray.orig_y;
		return ;
	}
}

void		calc_distance_with_wall(t_env_cub3d *env)
{
	if (env->ray.side == 0)
	{
		if (env->ray.step_x < 0)
			env->ray.distance = (env->ray.map_x - env->player.pos.x + 1)
								/ env->ray.dir_x;
		else
			env->ray.distance = (env->ray.map_x - env->player.pos.x)
								/ env->ray.dir_x;
	}
	else
	{
		if (env->ray.step_y < 0)
			env->ray.distance = (env->ray.map_y - env->player.pos.y + 1)
								/ env->ray.dir_y;
		else
			env->ray.distance = (env->ray.map_y - env->player.pos.y)
								/ env->ray.dir_y;
	}
}

static void	launch_ray(t_env_cub3d *env, double angle_calc)
{
	env->ray.dir_x = cosf(angle_calc);
	env->ray.dir_y = sinf(angle_calc);
	env->ray.map_x = (int)env->ray.orig_x;
	env->ray.map_y = (int)env->ray.orig_y;
	env->ray.delta_dist_x = fabsf(1 / env->ray.dir_x);
	env->ray.delta_dist_y = fabsf(1 / env->ray.dir_y);
	env->ray.hit = 0;
	env->ray.side = 0;
	env->ray.limit = 0;
	define_step(env);
	found_wall(env);
}

void		raycasting(t_env_cub3d *env)
{
	double	angle;
	double	angle_calc;

	if (env->player.life.life > 0 && !env->player.is_end_level)
		special_movement(env);
	else if (!env->player.is_end_level)
		death_player(env);
	env->ray.angle_fisheye = FOV / 2;
	angle = env->player.pos.angle + env->ray.angle_fisheye;
	env->ray.orig_x = env->player.pos.x;
	env->ray.orig_y = env->player.pos.y;
	env->ray.column = 0;
	env->player.weapon.id_sprite_shoot = -1;
	while (env->ray.column < env->parse.resolution.x)
	{
		angle_calc = angle * DEG_TO_RAD;
		launch_ray(env, angle_calc);
		remove_fisheye(env);
		angle -= env->player.plane.angle_ray;
		env->ray.column++;
		env->ray.found_sprite = FALSE;
	}
	(env->nbr_sprite > 0) ? (draw_sprite(env)) : 0;
	if (env->parse.texture.hand[0] == '.' && env->player.life.life > 0)
		is_shoot(env);
}
