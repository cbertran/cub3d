/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mlx_event2.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cbertran <cbertran@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/31 15:19:04 by cbertran          #+#    #+#             */
/*   Updated: 2020/06/02 20:41:25 by cbertran         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

int		key_release(int key, t_env_cub3d *env)
{
	env->player.key[key] = 0;
	return (0);
}

int		key_press(int key, t_env_cub3d *env)
{
	env->player.key[key] = 1;
	return (0);
}

int		close_windows(int key, t_env_cub3d *env)
{
	(void)key;
	if (env->sound.music)
		close_sound_engine(env);
	mlx_destroy_window(env->window.mlx, env->window.window);
	free_memory(env);
	exit(EXIT_SUCCESS);
}

int		close_windows_cross(t_env_cub3d *env)
{
	mlx_create_image(env, &env->game);
	if (env->sound.music)
		close_sound_engine(env);
	mlx_destroy_window(env->window.mlx, env->window.window);
	free_memory(env);
	exit(EXIT_SUCCESS);
}

int		reduce_windows(t_env_cub3d *env)
{
	env->player.key[QUIT] = 0;
	env->player.key[UP] = 0;
	env->player.key[DOWN] = 0;
	env->player.key[LEFT] = 0;
	env->player.key[RIGHT] = 0;
	return (0);
}
