/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   malloc.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cbertran <cbertran@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/25 18:00:28 by cbertran          #+#    #+#             */
/*   Updated: 2020/03/04 17:04:26 by cbertran         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

void		malloc_map(t_env_cub3d *var)
{
	int	z;
	int	y;

	y = 0;
	z = ++var->width_map.width;
	if (!(var->map = malloc((var->width_map.line + 1) * (sizeof(char *)))))
		ft_error("Malloc error", 1);
	while (y < var->width_map.line)
	{
		if (!(var->map[y] = malloc((sizeof(char) * z + 1))))
			ft_error("Malloc error", 1);
		y++;
	}
	var->map[y] = NULL;
}
