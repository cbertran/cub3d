/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   bonus_sprite.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cbertran <cbertran@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/06 00:51:45 by cbertran          #+#    #+#             */
/*   Updated: 2020/05/29 00:01:03 by cbertran         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

/*
**  2: movible_sprite
**  3: ammunition --> n
**  4: health --> n
*/

static void	movible_sprite(t_env_cub3d *var)
{
	var->parse.type_sprite = 1;
	var->parse.type[var->parse.y] = 1;
	var->parse.number[var->parse.y] = 15;
}

static void	health(t_env_cub3d *var, int number)
{
	var->parse.type_sprite = 2;
	var->parse.type[var->parse.y] = 2;
	var->parse.number[var->parse.y] = number;
}

static void	ammunition(t_env_cub3d *var, int number)
{
	var->parse.type_sprite = 3;
	var->parse.type[var->parse.y] = 3;
	var->parse.number[var->parse.y] = number;
}

void		bonus_sprite(t_env_cub3d *var, int x, char *s, char type)
{
	char	num[12];
	int		z;
	int		number;

	if (type == '2' && (s[x + 1] == ' ' || s[x + 1] == '\0'))
		movible_sprite(var);
	else if (type == '3' || type == '4')
	{
		z = 0;
		x++;
		while (s[x] == ' ')
			x++;
		while (s[x] >= '0' && s[x] <= '9')
			num[z++] = s[x++];
		num[z] = '\0';
		number = ft_atoi(num);
		(number <= 0) ? (ft_error("Incorrect value for bonus", 18)) : 0;
		(type == '3') ? (ammunition(var, number)) : (health(var, number));
	}
}
