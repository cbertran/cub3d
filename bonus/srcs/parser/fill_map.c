/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fill_map.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cbertran <cbertran@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/16 22:45:44 by cbertran          #+#    #+#             */
/*   Updated: 2020/06/02 18:51:23 by cbertran         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

static void	player_position_start(t_env_cub3d *var, int x, int y, char c)
{
	var->width_map.see_place++;
	var->player.start_case = c;
	if (c == 'N')
		var->player.pos.angle = 180;
	else if (c == 'S')
		var->player.pos.angle = 0;
	else if (c == 'E')
		var->player.pos.angle = 270;
	else if (c == 'W')
		var->player.pos.angle = 90;
	var->player.pos.x = x + 0.5;
	var->player.pos.y = y + 0.5;
	var->player.x_start = var->player.pos.x;
	var->player.y_start = var->player.pos.y;
}

static void	fill_line(t_env_cub3d *var, char *line, int x)
{
	int	i;
	int	y;

	i = 0;
	y = 0;
	while (line[i] != '\0')
	{
		if (ck_le(line[i]) || line[i] == ' ' || line[i] == '0'
			|| line[i] == 'N' || line[i] == 'S' || line[i] == 'E'
			|| line[i] == 'W')
		{
			if (line[i] == 'N' || line[i] == 'S' || line[i] == 'E'
			|| line[i] == 'W')
				player_position_start(var, x, y, line[i]);
			var->map[x][y++] = line[i];
		}
		i++;
	}
	while (i < var->width_map.width)
	{
		var->map[x][y++] = ' ';
		i++;
	}
	var->map[x][y] = '\0';
}

static void	is_map(t_env_cub3d *var, char *line, int current_line)
{
	if (current_line >= var->width_map.line_number_map
		&& (current_line < (var->width_map.line_number_map
		+ var->width_map.line)))
		fill_line(var, line, var->width_map.x++);
	free(line);
}

static void	if_wall_not_defined(t_env_cub3d *var)
{
	int	i;

	if (!var->parse.if_wall_def)
	{
		var->wall[0].type = '0';
		var->wall[0].height = 0.0;
		var->wall[1].type = '1';
		var->wall[1].height = 1.0;
		var->wall[2].type = 'N';
		var->wall[2].height = 0.0;
		var->wall[3].type = 'S';
		var->wall[3].height = 0.0;
		var->wall[4].type = 'E';
		var->wall[4].height = 0.0;
		var->wall[5].type = 'W';
		var->wall[5].height = 0.0;
		i = 6;
		while (i < 32)
		{
			var->wall[i].type = '#';
			var->wall[i++].height = 0.0;
		}
	}
}

void		fill_map(t_env_cub3d *var, char *file)
{
	int		fd;
	int		ret;
	char	*line;
	int		current_line;

	fd = 0;
	line = NULL;
	var->width_map.x = 0;
	current_line = 0;
	if_wall_not_defined(var);
	(file) ? fd = open(file, O_RDONLY) : 0;
	malloc_map(var);
	var->width_map.line_buf = var->width_map.line;
	while ((ret = get_next_line(fd, &line)) > 0)
		is_map(var, line, current_line++);
	if (ret == 0)
		is_map(var, line, current_line++);
	(ret < 0) ? free(line) : 0;
	if (var->width_map.see_place < 1 || var->width_map.see_place > 1)
		ft_error("Ambiguous player placement", 8);
	close(fd);
	if_map_close(var);
}
