/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   calc_color.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cbertran <cbertran@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/27 16:13:31 by cbertran          #+#    #+#             */
/*   Updated: 2020/01/31 20:12:16 by cbertran         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

int		calc_color(t_color *color)
{
	unsigned char	red;
	unsigned char	green;
	unsigned char	blue;
	unsigned char	transparency;

	red = (unsigned char)color->r;
	green = (unsigned char)color->g;
	blue = (unsigned char)color->b;
	transparency = (unsigned char)color->a;
	return (transparency << 24 | red << 16 | green << 8 | blue);
}
