/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fill_path_sprite.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cbertran <cbertran@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/23 19:13:44 by cbertran          #+#    #+#             */
/*   Updated: 2020/06/07 19:48:50 by cbertran         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

static void	is_block(t_env_cub3d *var, char *s, char c, int x)
{
	static int	y;
	int			z;

	(!y) ? (y = 2) : 0;
	z = 2;
	while (s[x] == ' ')
		x++;
	while (var->is_hit[z] != '#')
		if (var->is_hit[z++] == c)
			return ;
	if (s[x] == '1' && (s[x + 1] == ' ' || s[x + 1] == '\0'))
		var->is_hit[y++] = c;
	else if (s[x] != '1')
		bonus_sprite(var, x, s, s[x]);
}

static int	get_id_map_2(t_env_cub3d *var, char *s, int x)
{
	int		z;

	var->parse.y = 0;
	while (var->parse.id[var->parse.y] != '0')
	{
		z = 0;
		while (var->parse.id[z] != '0')
		{
			if (s[x] == var->parse.id[z] || s[x] == '0')
				ft_error("Sprite identifier already taken", 18);
			z++;
		}
		var->parse.y++;
	}
	if (var->parse.y >= 34)
		ft_error("Maximum number of sprites reached", 18);
	var->parse.id[var->parse.y] = s[x];
	is_block(var, s, s[x], x + 1);
	return (var->parse.y);
}

static int	get_id_map(t_env_cub3d *var, char *s, int x)
{
	var->parse.y = 0;
	if (!var->parse.id[0])
		while (var->parse.y < 34)
			var->parse.id[var->parse.y++] = '0';
	while (s[x] == ' ')
		x++;
	var->parse.y = 0;
	if ((s[x + 1] == ' ' || s[x + 1] == '\0')
		&& (is_present(s[x], "23456789ABCDFGHIJKLMOPQRTUVXYZ")))
		return (get_id_map_2(var, s, x));
	else if (!is_present(s[x], "23456789ABCDFGHIJKLMOPQRTUVXYZ")
				&& s[x] != '\0' && s[x] != '1')
		ft_error("Sprite identifier incorrect", 18);
	while (var->parse.id[var->parse.y] != '0'
		|| var->parse.id[var->parse.y] == '2')
		var->parse.y++;
	var->parse.id[var->parse.y] = '2';
	is_block(var, s, '2', x);
	return (0);
}

static int	fill_path_sprite_else(char *s, int x)
{
	if (s[x + 1] == '\0' || (s[x] == 'S' && s[x + 1] != ' '))
	{
		if (s[x] == 'S' && s[x + 1] == 'K' && s[x + 2] == 'Y' && s[x + 3] == 'B'
			&& s[x + 4] == 'O' && s[x + 5] == 'X')
			return (1);
		else
			ft_error("Incorrect path for texture", 6);
	}
	return (0);
}

void		fill_path_sprite(t_env_cub3d *env, char *s, t_texture *var, int x)
{
	static int	is_sprite;
	int			y;
	char		save[BUFFER_SIZE];

	check_parse_sum_sprite(env);
	while (s[x])
	{
		if (s[x] == '.')
		{
			y = 0;
			save[y++] = s[x++];
			while (s[x] != ' ' && s[x])
				save[y++] = s[x++];
			save[y] = '\0';
			env->parse.id_map = get_id_map(env, s, x);
			ft_memcpy(var->sprite[env->parse.id_map], save, y);
			var->sprite[env->parse.id_map][y] = '\0';
			(!is_sprite) ? (is_sprite = var->texture_check++) : 0;
			var->type_sprite[env->parse.id_map] = env->parse.type_sprite;
			(env->parse.type_sprite != 1) ? (check_texture(save)) : 0;
			break ;
		}
		if (fill_path_sprite_else(s, x++))
			return ;
	}
}
