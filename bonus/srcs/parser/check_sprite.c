/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_sprite.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cbertran <cbertran@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/04/09 17:46:11 by cbertran          #+#    #+#             */
/*   Updated: 2020/05/29 00:07:58 by cbertran         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

/*
**    |1|
**   0|x|2
**    |3|   calcul basé sur la case 3
**          	& priorité à la case 3 en cas d'égalité OU en ordre décroissant
**				si trop élevé
*/

static void	found_height_sprite(t_env_cub3d *var, int i, int x, int y)
{
	char	map_case[4];

	map_case[0] = var->map[x - 1][y];
	map_case[1] = var->map[x][y - 1];
	map_case[2] = var->map[x + 1][y];
	map_case[3] = var->map[x][y + 1];
	if (map_case[3] != '1')
		var->sprite[i].map_case = map_case[3];
	else if (map_case[2] != '1')
		var->sprite[i].map_case = map_case[2];
	else if (map_case[1] != '1')
		var->sprite[i].map_case = map_case[1];
	else if (map_case[0] != '1')
		var->sprite[i].map_case = map_case[0];
	else
		var->sprite[i].map_case = '0';
	var->sprite[i].type = var->map[x][y];
	var->sprite[i].x = x + 0.5;
	var->sprite[i].y = y + 0.5;
	var->sprite[i].x_case = x + 0.5;
	var->sprite[i].y_case = y + 0.5;
	var->sprite[i].distance = 0;
	var->sprite[i].z = var->wall[get_wall(var, var->sprite[i].map_case)].height;
}

void		fill_sprite_struct(t_env_cub3d *var, int x, int y)
{
	static int	i;
	int			z;

	(!i) ? (i = 0) : 0;
	if (i < MAX_SPRITE)
	{
		found_height_sprite(var, i, x, y);
		z = 0;
		while (var->parse.id[z] != '0')
		{
			if (var->parse.id[z] == var->sprite[i].type)
			{
				var->sprite[i].tab_adress = z;
				var->sprite[i].sprite_type = var->parse.type[z];
				if (var->sprite[i].sprite_type == 1)
					var->sprite[i].health = 15;
				break ;
			}
			z++;
		}
		var->nbr_sprite++;
		i++;
	}
	else
		ft_error("Maximum numbers of sprites reached", 18);
}
