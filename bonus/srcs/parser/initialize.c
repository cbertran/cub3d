/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   initialize.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cbertran <cbertran@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/31 18:41:53 by cbertran          #+#    #+#             */
/*   Updated: 2020/06/05 02:41:41 by cbertran         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

int			check_start_parse(t_env_cub3d *var, char *line, char *sch)
{
	int	x;

	x = 0;
	while (line[x] != '\0')
	{
		if ((line[x] == ' ' && line[x + 1] == sch[0])
			|| (x == 0 && line[x] == sch[x]))
		{
			if (ft_strstr(line, sch))
			{
				var->parse.count_arg++;
				return (1);
			}
		}
		else if (line[x] != ' ')
			break ;
		x++;
	}
	return (0);
}

static void	reset_height_wall(t_env_cub3d *env)
{
	int	i;

	i = 0;
	while (i < 32)
	{
		env->wall[i].type = '#';
		env->wall[i++].height = 0.0;
	}
	i = 0;
	while (i < 35)
	{
		env->texture.sprite[i].type = 0;
		env->texture.sprite[i].value = 0;
		env->parse.texture.type_sprite[i++] = 0;
	}
	env->parse.check.resolution = 0;
	env->parse.check.north_texture = 0;
	env->parse.check.south_texture = 0;
	env->parse.check.east_texture = 0;
	env->parse.check.west_texture = 0;
	env->parse.check.sprite = 0;
	env->parse.check.floor = 0;
	env->parse.check.ceilling = 0;
}

static void	reset_player(t_env_cub3d *env)
{
	int	x;

	x = 0;
	env->player.dir.x = 0;
	env->player.dir.y = 0;
	env->player.pos.angle = 0;
	env->nbr_sprite = 0;
	env->player.key[UP] = 0;
	env->player.key[DOWN] = 0;
	env->player.key[LEFT] = 0;
	env->player.key[RIGHT] = 0;
	env->player.key[TURN_LEFT] = 0;
	env->player.key[TURN_RIGHT] = 0;
	env->player.key[SHIFT_LEFT] = 0;
	env->player.key[SHIFT_RIGHT] = 0;
	env->player.key[LOOK_UP] = 0;
	env->player.key[LOOK_DOWN] = 0;
	env->player.key[CROUCH] = 0;
	env->player.key[JUMP] = 0;
	env->player.key[QUIT] = 0;
	while (x < 63)
		env->is_hit[x++] = '#';
	env->is_hit[0] = ' ';
	env->is_hit[1] = '1';
}

static void	reset_fill_bonus(t_env_cub3d *env)
{
	env->parse.minimap_back.r = 160;
	env->parse.minimap_back.g = 82;
	env->parse.minimap_back.b = 45;
	env->parse.minimap_back.a = 0;
	env->parse.minimap_back.check = 1;
	env->parse.minimap_back.hexa = calc_color(&env->parse.minimap_back);
	env->parse.minimap_block.r = 255;
	env->parse.minimap_block.g = 165;
	env->parse.minimap_block.b = 79;
	env->parse.minimap_block.a = 0;
	env->parse.minimap_block.check = 1;
	env->parse.minimap_block.hexa = calc_color(&env->parse.minimap_block);
	env->parse.minimap_player.r = 63;
	env->parse.minimap_player.g = 135;
	env->parse.minimap_player.b = 204;
	env->parse.minimap_player.a = 0;
	env->parse.minimap_player.check = 1;
	env->parse.minimap_player.hexa = calc_color(&env->parse.minimap_player);
}

void		reset_fill_parse(t_env_cub3d *env)
{
	env->parse.count_arg = 0;
	env->width_map.line = 0;
	env->width_map.line_buf = 0;
	env->width_map.is_player = 0;
	env->width_map.player_is_closing = 0;
	env->width_map.line_number_map = 0;
	env->width_map.line_number_map_boolean = 0;
	env->width_map.width = 0;
	env->width_map.check_width = 0;
	env->width_map.see_place = 0;
	env->width_map.boolean = 0;
	env->parse.if_wall_def = FALSE;
	env->parse.is_hud = FALSE;
	env->parse.texture.texture_check = 0;
	env->parse.ceilling.a = 0;
	env->parse.floor.a = 0;
	env->parse.hud.r = 64;
	env->parse.hud.g = 64;
	env->parse.hud.b = 64;
	env->parse.hud.a = 0;
	env->parse.hud.check = 1;
	env->parse.hud.hexa = calc_color(&env->parse.hud);
	reset_fill_bonus(env);
	reset_height_wall(env);
	reset_player(env);
}
