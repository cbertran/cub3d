/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   is_hole.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cbertran <cbertran@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/08 18:53:20 by cbertran          #+#    #+#             */
/*   Updated: 2020/03/08 19:07:00 by cbertran         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

static void	is_escape_verification_y(t_env_cub3d *var, int x, int y,
										int is_wall)
{
	int		y_map;

	y_map = y;
	while (y_map >= 0 &&
			(var->map[x][y_map] == ' ' || var->map[x][y_map] == '1'))
	{
		if (var->map[x][y_map] == '1' || y_map-- == 0)
		{
			is_wall = TRUE;
			break ;
		}
	}
	(is_wall == FALSE) ? ft_error("Incorrect map", 3) : (is_wall = FALSE);
	y_map = y;
	while (y_map <= var->width_map.width
			&& (var->map[x][y_map] == ' ' || var->map[x][y_map] == '1'))
	{
		if (var->map[x][y_map] == '1' || y_map++ == var->width_map.width - 1)
		{
			is_wall = TRUE;
			break ;
		}
	}
	(is_wall == 0) ? ft_error("Incorrect map", 3) : 0;
}

static void	is_escape_verification_x(t_env_cub3d *var, int x, int y,
										int is_wall)
{
	int x_map;

	x_map = x;
	while (x_map >= 0
			&& (var->map[x_map][y] == ' ' || var->map[x_map][y] == '1'))
	{
		if (var->map[x_map][y] == '1' || x_map-- == 0)
		{
			is_wall = TRUE;
			break ;
		}
	}
	(is_wall == FALSE) ? ft_error("Incorrect map", 3) : (is_wall = FALSE);
	x_map = x;
	while (x_map < var->width_map.line
			&& (var->map[x_map][y] == ' ' || var->map[x_map][y] == '1'))
	{
		if (var->map[x_map][y] == '1' || x_map++ == var->width_map.line - 1)
		{
			is_wall = TRUE;
			break ;
		}
	}
	(is_wall == FALSE) ? ft_error("Incorrect map", 3) : 0;
}

void		is_correct_hole(t_env_cub3d *var, int x, int y)
{
	int		is_wall;

	is_wall = FALSE;
	is_escape_verification_y(var, x, y, is_wall);
	is_escape_verification_x(var, x, y, is_wall);
}
