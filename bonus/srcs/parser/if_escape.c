/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   if_escape.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cbertran <cbertran@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/06 18:50:08 by cbertran          #+#    #+#             */
/*   Updated: 2020/03/09 17:23:44 by cbertran         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

static int	is_check(char c, char *str)
{
	while (*str)
	{
		if (*str == c)
			return (1);
		str++;
	}
	return (0);
}

static int	search_character(t_env_cub3d *var, int x, int y, int *is_wall)
{
	if (is_check(var->map[x][y], "NSEW"))
	{
		var->width_map.player_x = x;
		var->width_map.player_y = y;
		var->width_map.is_player = 1;
	}
	else if (is_check(var->map[x][y], "1"))
	{
		*is_wall = TRUE;
		return (1);
	}
	return (0);
}

static void	is_escape_verification_y(t_env_cub3d *var, int x, int y,
										int is_wall)
{
	int		y_map;

	y_map = y;
	while (y_map >= 0)
	{
		if (search_character(var, x, y_map--, &is_wall))
			break ;
	}
	(is_wall == FALSE) ? ft_error("Incorrect map", 3) : (is_wall = FALSE);
	y_map = y;
	while (y_map <= var->width_map.width)
	{
		if (search_character(var, x, y_map++, &is_wall))
			break ;
	}
	(is_wall == FALSE) ? ft_error("Incorrect map", 3) : 0;
}

static void	is_escape_verification_x(t_env_cub3d *var, int x, int y,
										int is_wall)
{
	int x_map;

	x_map = x;
	while (x_map >= 0)
	{
		if (search_character(var, x_map--, y, &is_wall))
			break ;
	}
	(is_wall == FALSE) ? ft_error("Incorrect map", 3) : (is_wall = FALSE);
	x_map = x;
	while (x_map < var->width_map.line)
	{
		if (search_character(var, x_map++, y, &is_wall))
			break ;
	}
	(is_wall == FALSE) ? ft_error("Incorrect map", 3) : 0;
}

void		is_escape(t_env_cub3d *var, int x, int y)
{
	int		is_wall;

	is_wall = FALSE;
	is_escape_verification_y(var, x, y, is_wall);
	is_escape_verification_x(var, x, y, is_wall);
	if (var->width_map.is_player)
		var->width_map.player_is_closing = 1;
}
