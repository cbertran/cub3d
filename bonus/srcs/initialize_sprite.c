/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   initialize_sprite.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cbertran <cbertran@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/15 18:35:19 by cbertran          #+#    #+#             */
/*   Updated: 2020/05/28 23:44:45 by cbertran         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

static void	open_texture_sprite(t_env_cub3d *env, t_file_tex *tex, char *s)
{
	tex->img = mlx_xpm_file_to_image(env->window.mlx, s,
										&tex->width, &tex->height);
	if (tex->img == NULL)
		ft_error("Fail to open texture(s)", 15);
	tex->address = (int *)mlx_get_data_addr(tex->img, &tex->bpp,
												&tex->line_length, &tex->end);
}

static void	animated_sprite_2(t_env_cub3d *env, int x)
{
	char *s;

	s = ft_strjoin_2(env->parse.texture.sprite[x], "/shoot.xpm");
	open_texture_sprite(env, &env->texture.sprite[x].shoot, s);
	free(s);
	s = ft_strjoin_2(env->parse.texture.sprite[x], "/walk_1.xpm");
	open_texture_sprite(env, &env->texture.sprite[x].walk_1, s);
	free(s);
	s = ft_strjoin_2(env->parse.texture.sprite[x], "/walk_2.xpm");
	open_texture_sprite(env, &env->texture.sprite[x].walk_2, s);
	free(s);
	s = ft_strjoin_2(env->parse.texture.sprite[x], "/walk_3.xpm");
	open_texture_sprite(env, &env->texture.sprite[x].walk_3, s);
	free(s);
	s = ft_strjoin_2(env->parse.texture.sprite[x], "/walk_4.xpm");
	open_texture_sprite(env, &env->texture.sprite[x].walk_4, s);
	free(s);
}

static void	animated_sprite_1(t_env_cub3d *env, int x)
{
	char *s;

	s = ft_strjoin_2(env->parse.texture.sprite[x], "/death_1.xpm");
	open_texture_sprite(env, &env->texture.sprite[x].death_1, s);
	free(s);
	s = ft_strjoin_2(env->parse.texture.sprite[x], "/death_2.xpm");
	open_texture_sprite(env, &env->texture.sprite[x].death_2, s);
	free(s);
	s = ft_strjoin_2(env->parse.texture.sprite[x], "/death_3.xpm");
	open_texture_sprite(env, &env->texture.sprite[x].death_3, s);
	free(s);
	s = ft_strjoin_2(env->parse.texture.sprite[x], "/death_4.xpm");
	open_texture_sprite(env, &env->texture.sprite[x].death_4, s);
	free(s);
	s = ft_strjoin_2(env->parse.texture.sprite[x], "/death_5.xpm");
	open_texture_sprite(env, &env->texture.sprite[x].death_5, s);
	free(s);
	s = ft_strjoin_2(env->parse.texture.sprite[x], "/fire_1.xpm");
	open_texture_sprite(env, &env->texture.sprite[x].fire_1, s);
	free(s);
	s = ft_strjoin_2(env->parse.texture.sprite[x], "/fire_2.xpm");
	open_texture_sprite(env, &env->texture.sprite[x].fire_2, s);
	free(s);
	animated_sprite_2(env, x);
}

static void	sprite_address_2(t_env_cub3d *env, int y, int cmp)
{
	env->texture.sprite[y].type = env->parse.type[y];
	env->texture.sprite[y].value = env->parse.number[y];
	if (env->parse.type[y] == 1)
		animated_sprite_1(env, y);
	else
	{
		if (cmp == 1)
			open_texture_sprite(env, &env->texture.sprite[y].alone,
									env->parse.texture.sprite[0]);
		else
			open_texture_sprite(env, &env->texture.sprite[y].alone,
									env->parse.texture.sprite[y]);
	}
}

void		get_sprite_adress(t_env_cub3d *env)
{
	int	x;
	int	y;
	int	cmp;

	x = 0;
	y = 0;
	while (env->parse.id[y] != '0')
	{
		if (env->parse.id[y] == '2')
		{
			sprite_address_2(env, y, 1);
			break ;
		}
		y++;
	}
	cmp = y;
	y = 1;
	while (env->parse.id[x] != '0')
	{
		if (x != cmp)
			sprite_address_2(env, x, 2);
		x++;
	}
}
