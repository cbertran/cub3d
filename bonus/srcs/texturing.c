/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   texturing.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cbertran <cbertran@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/13 16:13:24 by cbertran          #+#    #+#             */
/*   Updated: 2020/05/29 23:36:14 by cbertran         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

int		ck_le(char c)
{
	if ((c == '1') || (c >= 'a' && c <= 'z'))
		return (1);
	else if ((c >= '2' && c <= '9') || (c >= 'A' && c <= 'D')
		|| (c >= 'F' && c <= 'M') || (c >= 'O' && c <= 'R')
		|| (c >= 'T' && c <= 'V') || (c >= 'X' && c <= 'Z'))
		return (2);
	return (0);
}

void	remove_fisheye(t_env_cub3d *env)
{
	env->ray.distance_for_texture = env->ray.distance;
	env->ray.distance *= cos(env->ray.angle_fisheye * DEG_TO_RAD);
	env->ray.angle_fisheye -= env->player.plane.angle_ray;
}

void	draw_texture(t_env_cub3d *env, t_file_tex *tex, int column, int i)
{
	int	color;

	tex->tex_y = (int)tex->tex_pos & (tex->height - 1);
	tex->tex_pos += tex->step;
	color = color_from_image(tex, tex->tex_x, tex->tex_y);
	my_mlx_pixel_put(&env->game, column, i, color);
}

void	texture_orientation(t_env_cub3d *env, int column, int i)
{
	if (env->ray.side == 0 && (ck_le(env->ray.map_case) == 1))
	{
		if (env->ray.step_x < 0)
			draw_texture(env, &env->texture.north, column, i);
		else
			draw_texture(env, &env->texture.south, column, i);
	}
	else if (ck_le(env->ray.map_case) == 1)
	{
		if (env->ray.step_y < 0)
			draw_texture(env, &env->texture.west, column, i);
		else
			draw_texture(env, &env->texture.east, column, i);
	}
}
