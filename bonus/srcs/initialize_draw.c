/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   initialize_draw.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cbertran <cbertran@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/01 17:39:36 by cbertran          #+#    #+#             */
/*   Updated: 2020/06/02 15:48:28 by cbertran         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

static void		is_floor_ceilling_texture(t_env_cub3d *env, int type)
{
	if (type == 1)
	{
		env->texture.floor.img = mlx_xpm_file_to_image(env->window.mlx,
			env->parse.texture.floor, &env->texture.floor.width,
			&env->texture.floor.height);
		if (env->texture.floor.img == NULL)
			ft_error("Fail to open texture(s)", 15);
		env->texture.floor.address = (int *)mlx_get_data_addr(
			env->texture.floor.img, &env->texture.floor.bpp,
			&env->texture.floor.line_length, &env->texture.floor.end);
	}
	else if (type == 2)
	{
		env->texture.ceilling.img = mlx_xpm_file_to_image(env->window.mlx,
			env->parse.texture.ceilling, &env->texture.ceilling.width,
			&env->texture.ceilling.height);
		if (env->texture.ceilling.img == NULL)
			ft_error("Fail to open texture(s)", 15);
		env->texture.ceilling.address = (int *)mlx_get_data_addr(
			env->texture.ceilling.img, &env->texture.ceilling.bpp,
			&env->texture.ceilling.line_length, &env->texture.ceilling.end);
	}
}

static	void	get_adress_texture(t_env_cub3d *env)
{
	env->texture.north.address = (int *)mlx_get_data_addr(
		env->texture.north.img, &env->texture.north.bpp,
		&env->texture.north.line_length, &env->texture.north.end);
	env->texture.south.address = (int *)mlx_get_data_addr(
		env->texture.south.img, &env->texture.south.bpp,
		&env->texture.south.line_length, &env->texture.south.end);
	env->texture.east.address = (int *)mlx_get_data_addr(env->texture.east.img,
		&env->texture.east.bpp, &env->texture.east.line_length,
		&env->texture.east.end);
	env->texture.west.address = (int *)mlx_get_data_addr(env->texture.west.img,
		&env->texture.west.bpp, &env->texture.west.line_length,
		&env->texture.west.end);
}

static void		hud_number(t_env_cub3d *env, char *s)
{
	int		count;
	int		x;
	int		y;
	char	*s1;

	count = 0;
	x = 2;
	y = 0;
	while (s[count] != '.')
		count++;
	while (s[count] != '/')
		count--;
	count += 3;
	if (!(s1 = malloc(sizeof(char) * (count + 1))))
		ft_error("Error with malloc", 4);
	s1[0] = '.';
	s1[1] = '/';
	while (x < count)
		s1[x++] = s[y++];
	s1[x] = '\0';
	get_number_adress(env, s1);
	free(s1);
}

void			open_texture(t_env_cub3d *env, char *s)
{
	(env->parse.is_hud == TRUE) ? (hud_number(env, s)) : 0;
	env->texture.north.img = mlx_xpm_file_to_image(env->window.mlx,
		env->parse.texture.north, &env->texture.north.width,
		&env->texture.north.height);
	env->texture.south.img = mlx_xpm_file_to_image(env->window.mlx,
		env->parse.texture.south, &env->texture.south.width,
		&env->texture.south.height);
	env->texture.east.img = mlx_xpm_file_to_image(env->window.mlx,
		env->parse.texture.east, &env->texture.east.width,
		&env->texture.east.height);
	env->texture.west.img = mlx_xpm_file_to_image(env->window.mlx,
		env->parse.texture.west, &env->texture.west.width,
		&env->texture.west.height);
	get_sprite_adress(env);
	get_skybox_adress(env);
	if (env->parse.texture.hand[0] == '.')
		get_hand_adress(env);
	if (env->texture.north.img == NULL || env->texture.south.img == NULL
		|| env->texture.east.img == NULL || env->texture.west.img == NULL)
		ft_error("Fail to open texture(s)", 15);
	get_adress_texture(env);
	if (env->parse.texture.ceilling[0] == '.')
		is_floor_ceilling_texture(env, 2);
	if (env->parse.texture.floor[0] == '.')
		is_floor_ceilling_texture(env, 1);
}
