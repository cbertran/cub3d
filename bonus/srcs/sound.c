/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sound.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cbertran <cbertran@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/04/06 16:09:40 by cbertran          #+#    #+#             */
/*   Updated: 2020/05/29 17:39:29 by cbertran         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

static void	initialize_sprite_sound(t_env_cub3d *env, int channel)
{
	int		i;
	int		id;
	char	*sound;

	i = 0;
	while (i < env->nbr_sprite)
	{
		if (env->sprite[i].sprite_type == 1 && env->sound.music_link[0] == '.')
		{
			id = env->sprite[i].tab_adress;
			sound = ft_strjoin_2(env->parse.texture.sprite[id], "/death.ogg");
			env->sprite[i].death = Mix_LoadWAV(sound);
			free(sound);
			sound = ft_strjoin_2(env->parse.texture.sprite[id], "/injured.ogg");
			env->sprite[i].injured = Mix_LoadWAV(sound);
			free(sound);
			sound = ft_strjoin_2(env->parse.texture.sprite[id], "/shoot.ogg");
			env->sprite[i].shoot = Mix_LoadWAV(sound);
			free(sound);
			Mix_Volume(channel, MIX_MAX_VOLUME);
			env->sprite[i].channel = channel++;
		}
		i++;
	}
}

static void	initialize_player_sound(t_env_cub3d *env)
{
	char	*sound;

	sound = ft_strjoin_2(env->sound.music_link, "/bang.ogg");
	env->sound.bang = Mix_LoadWAV(sound);
	free(sound);
	sound = ft_strjoin_2(env->sound.music_link, "/empty.ogg");
	env->sound.empty = Mix_LoadWAV(sound);
	free(sound);
	sound = ft_strjoin_2(env->sound.music_link, "/pick_up.ogg");
	env->sound.pick_up = Mix_LoadWAV(sound);
	free(sound);
	sound = ft_strjoin_2(env->sound.music_link, "/injured.ogg");
	env->sound.injured = Mix_LoadWAV(sound);
	free(sound);
	sound = ft_strjoin_2(env->sound.music_link, "/death.ogg");
	env->sound.death = Mix_LoadWAV(sound);
	free(sound);
}

void		initialize_sound_engine(t_env_cub3d *env)
{
	char	*music;

	music = ft_strjoin_2(env->sound.music_link, "/music.ogg");
	SDL_Init(SDL_INIT_AUDIO);
	(Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, MIX_DEFAULT_CHANNELS,
		1024) == -1) ? (ft_error("Fail create sound", 19)) : 0;
	Mix_AllocateChannels((3 + env->nbr_sprite));
	Mix_Volume(1, MIX_MAX_VOLUME);
	Mix_Volume(2, MIX_MAX_VOLUME);
	Mix_Volume(3, MIX_MAX_VOLUME);
	env->sound.music = Mix_LoadMUS(music);
	free(music);
	initialize_player_sound(env);
	initialize_sprite_sound(env, 4);
}

void		play_music(t_env_cub3d *env)
{
	Mix_PlayMusic(env->sound.music, -1);
}

void		play_sound(t_env_cub3d *env, int channel, Mix_Chunk *chunk)
{
	if (env->sound.music_link[0] == '.')
		Mix_PlayChannel(channel, chunk, 0);
}
