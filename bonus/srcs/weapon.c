/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   weapon.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cbertran <cbertran@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/03 02:23:23 by cbertran          #+#    #+#             */
/*   Updated: 2020/05/29 16:56:21 by cbertran         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

static void	shooting(t_env_cub3d *env)
{
	if (env->player.weapon.ammunition > 0)
		env->player.weapon.ammunition--;
}

static void	is_shoot_2(t_env_cub3d *env, int tick, int wyn)
{
	if (wyn == 1 && tick == 1)
	{
		env->player.weapon.is_shoot = TRUE;
		if (env->player.weapon.ammunition > 0)
			play_sound(env, 1, env->sound.bang);
		else
			play_sound(env, 1, env->sound.empty);
	}
	else if (wyn == 2 && env->player.weapon.current_img == 0)
	{
		hand(env, &env->player.weapon.weapon_1);
		(tick == 5) ? (env->player.weapon.current_img = 1) : 0;
	}
	else if (wyn == 3)
	{
		hand(env, &env->player.weapon.weapon_0);
		env->player.weapon.current_img = 0;
		env->player.weapon.is_shoot = FALSE;
	}
}

static void	is_shoot_3(t_env_cub3d *env)
{
	if (env->player.weapon.ammunition <= 0)
		hand(env, &env->player.weapon.weapon_2);
	else
		hand(env, &env->player.weapon.weapon_3);
}

void		is_shoot(t_env_cub3d *env)
{
	static int	boolean;
	static int	tick;

	(!boolean) ? (boolean = FALSE) : 0;
	(!tick) ? (tick = 0) : 0;
	(env->player.key[SHOOT]) ? (boolean = TRUE) : 0;
	if (boolean == TRUE)
	{
		is_shoot_2(env, tick, 1);
		is_shoot_2(env, tick, 2);
		if (env->player.weapon.current_img == 1)
		{
			is_shoot_3(env);
			if (tick == 10)
			{
				env->player.weapon.current_img = 0;
				tick = 0;
				(env->player.weapon.is_shoot) ? (shooting(env)) : 0;
				boolean = FALSE;
			}
		}
		tick++;
	}
	else
		is_shoot_2(env, tick, 3);
}
