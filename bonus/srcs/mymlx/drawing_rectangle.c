/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   drawing_rectangle.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cbertran <cbertran@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/23 16:07:56 by cbertran          #+#    #+#             */
/*   Updated: 2020/01/30 22:30:33 by cbertran         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

static void	border_one(t_image *data, t_rectangle *rect, int y, int b)
{
	int	a;
	int	sx;
	int	sy;

	a = rect->x + rect->width;
	while (y < b)
	{
		sy = y * data->line_length;
		sx = rect->x;
		while (sx < a)
			data->address[sy + sx++] = rect->color;
		y++;
	}
}

static void	border_two(t_image *data, t_rectangle *rect, int a, int wyn)
{
	int	b;
	int	y;
	int	sx;
	int	sy;

	b = (rect->y + rect->height) - rect->border;
	y = rect->y + rect->border;
	while (y < b)
	{
		sy = y * data->line_length;
		if (wyn == 1)
			sx = rect->x;
		else
			sx = (rect->x + rect->width) - rect->border;
		while (sx < a)
			data->address[sy + sx++] = rect->color;
		y++;
	}
}

static void	rectangle_border(t_image *data, t_rectangle *rect)
{
	border_one(data, rect, rect->y, (rect->y + rect->border));
	border_one(data, rect, ((rect->y + rect->height) - rect->border),
		(rect->y + rect->height));
	border_two(data, rect, (rect->x + rect->border), 1);
	border_two(data, rect, (rect->x + rect->width), 2);
}

static void	rectangle_fill(t_image *data, t_rectangle *rect)
{
	int sx;
	int	sy;
	int	a;
	int	b;

	a = rect->x + rect->width;
	b = rect->y + rect->height;
	while (rect->y < b)
	{
		sy = rect->y * data->line_length;
		sx = rect->x;
		while (sx < a)
			data->address[sy + sx++] = rect->color;
		rect->y++;
	}
}

void		mlx_draw_rectangle(t_env_cub3d *env, t_image *data,
	t_rectangle *rect)
{
	(rect->x < 0) ? rect->x = 0 : 0;
	(rect->y < 0) ? rect->y = 0 : 0;
	(rect->height < 0) ? rect->height = 0 : 0;
	(rect->width < 0) ? rect->width = 0 : 0;
	if ((rect->y + rect->height) > env->parse.resolution.y)
		rect->height -= rect->height - (env->parse.resolution.y - rect->y);
	if ((rect->x + rect->width) > env->parse.resolution.x)
		rect->width -= rect->height - (env->parse.resolution.x - rect->x);
	if (rect->border <= 0)
		rectangle_fill(data, rect);
	else if (rect->border > 0)
		rectangle_border(data, rect);
}
