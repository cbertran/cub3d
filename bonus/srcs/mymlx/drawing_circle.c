/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   drawing_circle.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cbertran <cbertran@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/25 19:59:24 by cbertran          #+#    #+#             */
/*   Updated: 2020/01/31 20:28:39 by cbertran         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

static void	draw_circle(t_circle *cir, int *d, int *x, int *y)
{
	if (*d >= 2 * *x)
		*d -= 2 * *x++ + 1;
	else if (*d < 2 * (cir->radius - *y))
		*d += 2 * *y-- - 1;
	else
		*d += 2 * (*y-- - *x++ - 1);
}

static void	draw_circle_test(t_circle *cir, t_env_cub3d *env, int *d)
{
	*d = cir->radius - 1;
	(cir->x < 0) ? cir->x = 0 : 0;
	(cir->y < 0) ? cir->y = 0 : 0;
	(cir->radius < 0) ? cir->radius = 0 : 0;
	if ((cir->y + cir->radius) > env->parse.resolution.y)
		cir->radius -= cir->radius - (env->parse.resolution.y - cir->y);
	if ((cir->x + cir->radius) > env->parse.resolution.x)
		cir->radius -= cir->radius - (env->parse.resolution.x - cir->x);
}

static void	draw_circle_algo(t_circle *cir, t_image *i, int *x, int *y)
{
	my_mlx_pixel_put(i, (cir->x_center + *x), (cir->y_center + *y), cir->color);
	my_mlx_pixel_put(i, (cir->x_center + *y), (cir->y_center + *x), cir->color);
	my_mlx_pixel_put(i, (cir->x_center - *x), (cir->y_center + *y), cir->color);
	my_mlx_pixel_put(i, (cir->x_center - *y), (cir->y_center + *x), cir->color);
	my_mlx_pixel_put(i, (cir->x_center + *x), (cir->y_center - *y), cir->color);
	my_mlx_pixel_put(i, (cir->x_center + *y), (cir->y_center - *x), cir->color);
	my_mlx_pixel_put(i, (cir->x_center - *x), (cir->y_center - *y), cir->color);
	my_mlx_pixel_put(i, (cir->x_center - *y), (cir->y_center - *x), cir->color);
}

void		mlx_draw_circle(t_env_cub3d *env, t_image *data, t_circle *cir)
{
	int	x;
	int y;
	int d;

	x = 0;
	cir->y_center = cir->y + cir->radius / 2;
	y = cir->radius;
	cir->x_center = cir->x + cir->radius / 2;
	draw_circle_test(cir, env, &d);
	if (cir->border <= 0)
	{
		while (y >= x)
		{
			draw_circle_algo(cir, data, &x, &y);
			draw_circle(cir, &d, &x, &y);
		}
	}
}
