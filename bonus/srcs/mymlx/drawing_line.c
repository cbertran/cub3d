/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   drawing_line.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cbertran <cbertran@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/26 14:40:05 by cbertran          #+#    #+#             */
/*   Updated: 2020/01/31 20:13:27 by cbertran         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

void	mlx_draw_line(t_env_cub3d *env, t_image *data, t_line *line)
{
	(void)env;
	if (line->x1 > line->x2)
	{
		if (line->y1 > line->y2)
			while (line->x1 > line->x2 && line->y1 > line->y2)
				my_mlx_pixel_put(data, line->x1--, line->y1--, line->color);
		else
			while (line->x1 > line->x2 && line->y2 > line->y1)
				my_mlx_pixel_put(data, line->x1--, line->y2--, line->color);
	}
	else
	{
		if (line->y1 > line->y2)
			while (line->x2 > line->x1 && line->y1 > line->y2)
				my_mlx_pixel_put(data, line->x2--, line->y1--, line->color);
		else
			while (line->x2 > line->x1 && line->y2 > line->y1)
				my_mlx_pixel_put(data, line->x2--, line->y2--, line->color);
	}
}
