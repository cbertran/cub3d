/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   generate_image.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cbertran <cbertran@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/24 18:48:50 by cbertran          #+#    #+#             */
/*   Updated: 2020/06/05 14:34:52 by cbertran         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

void	mlx_initialize(t_env_cub3d *env, char *s)
{
	int	x_max;
	int	y_max;

	if (!(env->window.mlx = mlx_init()))
		ft_error("Failed to initialize window", 17);
	mlx_get_screen_size(env->window.mlx, &x_max, &y_max);
	if (env->parse.resolution.x > x_max)
		env->parse.resolution.x = x_max;
	if (env->parse.resolution.y > y_max)
		env->parse.resolution.y = y_max;
	if (env->is_bmp == FALSE)
	{
		if (!(env->window.window = mlx_new_window(env->window.mlx,
						env->parse.resolution.x, env->parse.resolution.y, s)))
			ft_error("Failed to create window", 17);
	}
}

void	mlx_create_image(t_env_cub3d *env, t_image *img)
{
	int	x;
	int	y;

	x = env->parse.resolution.x;
	y = env->parse.resolution.y;
	if (!(img->img = mlx_new_image(env->window.mlx, x, y)))
		ft_error("Failed to create image pointer", 17);
	if (!(img->address = (int *)mlx_get_data_addr(img->img, &img->bpp,
						&img->line_length, &img->end)))
		ft_error("Failed to get image pointer", 17);
	img->line_length /= 4;
}
