/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_color.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cbertran <cbertran@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/16 21:10:43 by cbertran          #+#    #+#             */
/*   Updated: 2020/04/04 20:26:11 by cbertran         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

int	color_from_image(t_file_tex *img, int x, int y)
{
	return (*(img->address + abs(x) + (abs(y) * img->width)));
}
