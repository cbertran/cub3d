/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   player.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cbertran <cbertran@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/28 19:40:28 by cbertran          #+#    #+#             */
/*   Updated: 2020/05/31 14:57:16 by cbertran         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

static void	reset_game(t_env_cub3d *env)
{
	int	x;
	int	tab;

	x = 0;
	env->player.pos.x = env->player.x_start;
	env->player.pos.y = env->player.y_start;
	env->player.plane.z_position = 0.5;
	env->player.life.life = 100;
	env->player.weapon.ammunition = 35;
	while (x < env->nbr_sprite)
	{
		tab = env->sprite[x].tab_adress;
		if (env->sprite[x].sprite_type == -1)
			env->sprite[x].sprite_type = env->parse.type[tab];
		else if (env->sprite[x].sprite_type == 1)
		{
			env->sprite[x].health = 15;
			env->sprite[x].x = env->sprite[x].x_case + 0.5;
			env->sprite[x].y = env->sprite[x].y_case + 0.5;
		}
		x++;
	}
}

void		death_player(t_env_cub3d *env)
{
	static int		frame;
	static double	delete;

	if (!frame)
	{
		frame = 0;
		play_sound(env, 3, env->sound.death);
	}
	(!delete) ? (delete = 0.4 / 15) : 0;
	if (frame < 15)
		env->player.plane.z_position -= delete;
	else if (frame == 25)
	{
		if (env->player.start_case == 'N')
			env->player.pos.angle = 180;
		else if (env->player.start_case == 'S')
			env->player.pos.angle = 0;
		else if (env->player.start_case == 'E')
			env->player.pos.angle = 270;
		else if (env->player.start_case == 'W')
			env->player.pos.angle = 90;
		reset_game(env);
		frame = 0;
	}
	frame++;
}

/*
**	2 : health - 3 : ammunition
*/

static void	bonus_2(t_env_cub3d *env, t_sprite *sprite)
{
	int	tab;

	tab = sprite->tab_adress;
	if (sprite->sprite_type == 2)
	{
		env->player.life.life += env->parse.number[tab];
		sprite->sprite_type = -1;
	}
	else if (sprite->sprite_type == 3)
	{
		env->player.weapon.ammunition += env->parse.number[tab];
		sprite->sprite_type = -1;
	}
	if (env->sound.music_link[0] == '.')
		play_sound(env, 3, env->sound.pick_up);
}

static void	is_ending(t_env_cub3d *env)
{
	t_number		place;
	t_screen_number	var;

	var.width = env->parse.resolution.x / 2;
	var.height = env->parse.resolution.y / 4;
	place.center_x = env->parse.resolution.x / 2;
	place.center_y = env->parse.resolution.y / 2;
	place.x = place.center_x - (var.width / 2);
	place.y = place.center_y - (var.height / 2);
	place.x_end = place.center_x + (var.width / 2);
	place.y_end = place.center_y + (var.height / 2);
	draw_number(env, &var, &place, &env->player.number.finish);
}

void		bonus(t_env_cub3d *env)
{
	static int	frame = 0;
	int			x;

	x = 0;
	if (is_present(env->player.map_case, "23456789ABCDFGHIJKLMOPQRTUVXYZ"))
	{
		while (env->sprite[x].type != '0' && x < env->nbr_sprite)
		{
			if ((env->sprite[x].x_case == (int)env->player.pos.x
				&& env->sprite[x].y_case == (int)env->player.pos.y)
				&& (env->sprite[x].sprite_type == 2
				|| env->sprite[x].sprite_type == 3))
				bonus_2(env, &env->sprite[x]);
			x++;
		}
	}
	else if (env->player.map_case == 'e')
	{
		env->player.is_end_level = TRUE;
		if (frame < 20)
			is_ending(env);
		else
			close_windows(QUIT, env);
		frame++;
	}
}
