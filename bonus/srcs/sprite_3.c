/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sprite_3.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cbertran <cbertran@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/27 14:35:26 by cbertran          #+#    #+#             */
/*   Updated: 2020/05/29 16:55:11 by cbertran         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

void	clean_sprite_struct(t_env_cub3d *env, int i)
{
	int column;
	int	count_column;
	int	x1;
	int	x2;

	column = env->sprite[i].start_x;
	count_column = column;
	x1 = env->sprite[i].start_y;
	(x1 < 0) ? (x1 = 0) : 0;
	while (column < env->sprite[i].end_x)
	{
		x2 = env->sprite[i].end_y;
		if (x2 > env->sprite[i].height[column])
			x2 = env->sprite[i].height[column];
		if (x1 > x2 || x1 > env->screen_height)
			count_column++;
		column++;
	}
	if (count_column == env->sprite[i].end_x)
		env->sprite[i].if_see = 0;
}

void	death_sprite(t_env_cub3d *env, int id, int i)
{
	if (env->sprite[i].frame >= 0)
	{
		env->sprite[i].frame = -21;
		play_sound(env, env->sprite[i].channel, env->sprite[i].death);
	}
	if (env->sprite[i].frame >= -21 && env->sprite[i].frame <= -17)
		env->sprite[i].link = &env->texture.sprite[id].death_1;
	else if (env->sprite[i].frame >= -16 && env->sprite[i].frame <= -13)
		env->sprite[i].link = &env->texture.sprite[id].death_2;
	else if (env->sprite[i].frame >= -12 && env->sprite[i].frame <= -9)
		env->sprite[i].link = &env->texture.sprite[id].death_3;
	else if (env->sprite[i].frame >= -8 && env->sprite[i].frame <= -5)
		env->sprite[i].link = &env->texture.sprite[id].death_4;
	else if (env->sprite[i].frame >= -4 && env->sprite[i].frame <= -1)
		env->sprite[i].link = &env->texture.sprite[id].death_5;
	if (env->sprite[i].frame == -1)
		env->sprite[i].health = -100;
}

void	fire_sprite(t_env_cub3d *env, int id, int i)
{
	if (env->sprite[i].frame > 0)
	{
		env->sprite[i].frame = -17;
		play_sound(env, env->sprite[i].channel, env->sprite[i].shoot);
		play_sound(env, 2, env->sound.injured);
		env->player.life.life -= 12;
	}
	if (env->sprite[i].frame >= -17 && env->sprite[i].frame <= -12)
		env->sprite[i].link = &env->texture.sprite[id].fire_1;
	else if (env->sprite[i].frame >= -11 && env->sprite[i].frame <= -6)
		env->sprite[i].link = &env->texture.sprite[id].fire_2;
	else if (env->sprite[i].frame >= -5 && env->sprite[i].frame <= -1)
		env->sprite[i].link = &env->texture.sprite[id].fire_1;
}

void	moving_sprite(t_env_cub3d *env, int id, int i)
{
	if (env->sprite[i].frame >= 0 && env->sprite[i].frame <= 7)
		env->sprite[i].link = &env->texture.sprite[id].walk_1;
	else if (env->sprite[i].frame > 7 && env->sprite[i].frame <= 14)
		env->sprite[i].link = &env->texture.sprite[id].walk_2;
	else if (env->sprite[i].frame > 21 && env->sprite[i].frame <= 28)
		env->sprite[i].link = &env->texture.sprite[id].walk_3;
	else if (env->sprite[i].frame > 35 && env->sprite[i].frame <= 42)
		env->sprite[i].link = &env->texture.sprite[id].walk_4;
	else if (env->sprite[i].frame > 42)
		env->sprite[i].frame = -1;
}
