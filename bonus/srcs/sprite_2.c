/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sprite_2.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cbertran <cbertran@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/15 23:46:38 by cbertran          #+#    #+#             */
/*   Updated: 2020/05/28 23:53:00 by cbertran         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

void	calc_dimension_sprite(t_env_cub3d *env, int i)
{
	double	ytmp;
	int		z_height;

	ytmp = env->player.pos.angle + (FOV / 2) - env->sprite[i].angle;
	if (env->sprite[i].angle > 270 && env->player.pos.angle < 90)
		ytmp = env->player.pos.angle + (FOV / 2) - env->sprite[i].angle + 360;
	if (env->sprite[i].angle < 90 && env->player.pos.angle > 270)
		ytmp = env->player.pos.angle + (FOV / 2) - env->sprite[i].angle - 360;
	env->sprite[i].center_x = ytmp * (env->parse.resolution.x) / FOV;
	env->sprite[i].size = (0.8 / env->sprite[i].distance
							* env->player.plane.distance_plane);
	env->sprite[i].start_x = env->sprite[i].center_x - env->sprite[i].size / 2;
	(env->sprite[i].start_x < 0) ? (env->sprite[i].start_x = 0) : 0;
	env->sprite[i].end_x = env->sprite[i].center_x + (env->sprite[i].size / 2);
	(env->sprite[i].end_x >= env->parse.resolution.x) ?
		(env->sprite[i].end_x = env->parse.resolution.x) : 0;
	z_height = (int)((env->sprite[i].z
					/ env->sprite[env->current_sprite].distance)
					* env->player.plane.distance_plane);
	env->sprite[i].end_y = ((env->player.plane.distance_plane
				/ env->sprite[i].distance) * env->player.plane.z_position
				+ env->player.plane.head_position) - z_height;
	env->sprite[i].start_y = env->sprite[i].end_y - env->sprite[i].size;
	clean_sprite_struct(env, env->current_sprite);
}

void	calc_texture_value_1(t_env_cub3d *env, t_file_tex *tex)
{
	int	id;

	id = env->current_sprite;
	tex->tex_x = (int)((256 * (env->column
		- (-env->sprite[id].size / 2 + env->sprite[id].center_x))
		* tex->width / env->sprite[id].size) / 256);
	tex->tex_pos = (env->sprite[id].start_y - env->sprite[id].end_y
					+ env->sprite[id].size) * tex->step;
	tex->step = 1.0 * tex->height / env->sprite[id].size;
	if_sprite(env, tex);
	env->column++;
}

void	calc_texture_value_2(t_env_cub3d *env, t_file_tex *tex, int line)
{
	int	col;

	tex->tex_y = (int)tex->tex_pos & (tex->height - 1);
	tex->tex_pos += tex->step;
	col = color_from_image(tex, tex->tex_x, tex->tex_y);
	if (col != 0x980088)
		my_mlx_pixel_put(&env->game, env->column, line, col);
}

void	check_is_shoot_value(t_env_cub3d *env)
{
	int	id;
	int	center_x;

	center_x = (int)env->player.plane.center_x;
	id = env->current_sprite;
	if (env->sprite[id].height[center_x] > env->sprite[id].start_y
		&& env->player.weapon.id_sprite_shoot == env->sprite[id].id
		&& env->player.weapon.is_shoot && env->player.weapon.ammunition > 0)
		env->sprite[id].is_shoot = TRUE;
	env->column = env->sprite[id].start_x;
}

void	if_sprite(t_env_cub3d *env, t_file_tex *tex)
{
	int		x1;
	int		x2;
	int		id;

	id = env->current_sprite;
	if (env->sprite[id].start_y < 0)
		env->sprite[id].start_y = 0;
	x1 = env->sprite[id].start_y;
	x2 = env->sprite[id].end_y;
	if (x1 > env->sprite[id].height[env->column])
		return ;
	if (x2 > env->sprite[id].height[env->column])
		x2 = env->sprite[id].height[env->column];
	while (x1 < x2 && x1 < env->screen_height)
		calc_texture_value_2(env, tex, x1++);
}
