/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   screenshot.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cbertran <cbertran@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/22 20:43:42 by cbertran          #+#    #+#             */
/*   Updated: 2020/05/02 18:40:34 by cbertran         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

static void	initialize(t_bmp *bmp, int size)
{
	bmp->adress = 54;
	bmp->bpp = 32;
	bmp->color_scheme = 1;
	bmp->header_size = 40;
	bmp->raw = size;
	bmp->res = 2835;
	bmp->size = size + 54;
}

static void	bmp_header(t_resolution *res, t_bmp *bmp)
{
	int i;

	i = 0;
	while (i < 54)
		bmp->header[i++] = 0;
	bmp->header[0] = 'B';
	bmp->header[1] = 'M';
	ft_memcpy(&bmp->header[2], &(bmp->size), sizeof(int));
	ft_memcpy(&bmp->header[10], &(bmp->adress), sizeof(int));
	ft_memcpy(&bmp->header[14], &(bmp->header_size), sizeof(int));
	ft_memcpy(&bmp->header[18], &(res->x), sizeof(int));
	ft_memcpy(&bmp->header[22], &(res->y), sizeof(int));
	ft_memcpy(&bmp->header[26], &(bmp->color_scheme), sizeof(short));
	ft_memcpy(&bmp->header[28], &(bmp->bpp), sizeof(short));
	ft_memcpy(&bmp->header[34], &(bmp->raw), sizeof(int));
	ft_memcpy(&bmp->header[38], &(bmp->res), sizeof(int));
	ft_memcpy(&bmp->header[42], &(bmp->res), sizeof(int));
}

static char	*bmp_pixels(t_env_cub3d *env, t_image *img, int size)
{
	int	x;
	int	y;
	int	*pxls;
	int	px;

	if (!(pxls = malloc(sizeof(int) * size)))
		return (NULL);
	x = 0;
	while (x < size)
		pxls[x++] = 0;
	y = 0;
	while (y < env->parse.resolution.y)
	{
		x = 0;
		while (x < env->parse.resolution.x)
		{
			px = (env->parse.resolution.y - 1 - y) * img->line_length + x;
			pxls[px] = img->address[x + (y * img->line_length)];
			x++;
		}
		y++;
	}
	return ((char *)pxls);
}

void		take_screenshot(t_env_cub3d *env, t_image *img)
{
	t_bmp	bmp;
	int		fd;
	int		size;
	char	*data;

	if (!(fd = open("picture.bmp", O_RDWR | O_CREAT, 77777)))
		ft_error("Fail to create bmp", 16);
	size = (env->parse.resolution.x * env->parse.resolution.y * 4);
	initialize(&bmp, size);
	bmp_header(&env->parse.resolution, &bmp);
	data = bmp_pixels(env, img, size);
	if (data != NULL)
	{
		write(fd, bmp.header, 54);
		write(fd, data, size);
		free(data);
	}
	else
		ft_error("Fail to create bmp", 16);
}
