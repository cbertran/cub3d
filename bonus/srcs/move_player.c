/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   move_player.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cbertran <cbertran@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/29 18:47:18 by cbertran          #+#    #+#             */
/*   Updated: 2020/05/29 17:04:54 by cbertran         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

static int	is_hit(t_env_cub3d *env, char c)
{
	static double	max_height;
	int				x;

	x = 0;
	(!max_height) ? (found_max_height(env, &max_height)) : 0;
	while (env->is_hit[x] != '#' && x < 63)
	{
		if (env->is_hit[x] == c)
			return (max_height + 2.0);
		x++;
	}
	return (0);
}

double		get_player_height(t_env_cub3d *env, double x, double y, int is_dif)
{
	double	futur_height;
	int		z;
	int		boolean;

	z = 0;
	boolean = FALSE;
	while (env->sprite[z].type != '0' && z < env->nbr_sprite)
	{
		if (env->sprite[z].x_case == (int)x
			&& env->sprite[z].y_case == (int)y)
		{
			if ((futur_height = is_hit(env, env->sprite[z].type)) == 0)
				futur_height = env->wall[get_wall(env,
								env->sprite[z].map_case)].height;
			boolean = TRUE;
		}
		z++;
	}
	if (boolean == FALSE)
		futur_height = env->wall[get_wall(env,
						env->map[(int)x][(int)y])].height;
	if (is_dif == TRUE)
		return (futur_height - env->player.difference);
	return (futur_height);
}

void		move_player(t_env_cub3d *env, t_image *img)
{
	t_rectangle	player;
	static int	a;
	static int	b;

	a = (env->parse.resolution.x - (env->parse.resolution.x / 6));
	b = (env->parse.resolution.y - (env->parse.resolution.y / 6));
	player.border = 0;
	player.color = env->parse.minimap_player.hexa;
	player.height = env->parse.resolution.y / 6 / env->width_map.line;
	player.width = env->parse.resolution.x / 6 / env->width_map.width;
	player.x = ((int)(env->player.pos.y) * player.width) + a;
	player.y = ((int)(env->player.pos.x) * player.height) + b;
	mlx_draw_rectangle(env, img, &player);
}
