/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   initialize_draw_2.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cbertran <cbertran@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/30 20:04:43 by cbertran          #+#    #+#             */
/*   Updated: 2020/05/31 19:18:31 by cbertran         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

void			get_skybox_adress(t_env_cub3d *env)
{
	if (env->parse.texture.skybox[0] == '.')
	{
		env->texture.skybox.img = mlx_xpm_file_to_image(env->window.mlx,
			env->parse.texture.skybox, &env->texture.skybox.width,
			&env->texture.skybox.height);
		if (env->texture.skybox.img == NULL)
			ft_error("Fail to open texture(s)", 15);
		env->texture.skybox.address = (int *)mlx_get_data_addr(
			env->texture.skybox.img, &env->texture.skybox.bpp,
			&env->texture.skybox.line_length, &env->texture.skybox.end);
	}
}

static void		get_hand_adress_2(t_env_cub3d *env)
{
	if (env->player.weapon.weapon_0.img == NULL
		|| env->player.weapon.weapon_1.img == NULL
		|| env->player.weapon.weapon_2.img == NULL
		|| env->player.weapon.weapon_3.img == NULL)
		ft_error("Fail to open texture(s)", 15);
	env->player.weapon.weapon_0.address = (int *)mlx_get_data_addr(
		env->player.weapon.weapon_0.img, &env->player.weapon.weapon_0.bpp,
		&env->player.weapon.weapon_0.line_length,
		&env->player.weapon.weapon_0.end);
	env->player.weapon.weapon_1.address = (int *)mlx_get_data_addr(
		env->player.weapon.weapon_1.img, &env->player.weapon.weapon_1.bpp,
		&env->player.weapon.weapon_1.line_length,
		&env->player.weapon.weapon_1.end);
	env->player.weapon.weapon_2.address = (int *)mlx_get_data_addr(
		env->player.weapon.weapon_2.img, &env->player.weapon.weapon_2.bpp,
		&env->player.weapon.weapon_2.line_length,
		&env->player.weapon.weapon_2.end);
	env->player.weapon.weapon_3.address = (int *)mlx_get_data_addr(
		env->player.weapon.weapon_3.img, &env->player.weapon.weapon_3.bpp,
		&env->player.weapon.weapon_3.line_length,
		&env->player.weapon.weapon_3.end);
	env->player.weapon.ammunition = 35;
	env->player.weapon.current_img = 0;
	env->player.weapon.is_shoot = 0;
}

void			get_hand_adress(t_env_cub3d *env)
{
	char	*hand;

	hand = ft_strjoin_2(env->parse.texture.hand, "/weapon_0.xpm");
	env->player.weapon.weapon_0.img = mlx_xpm_file_to_image(env->window.mlx,
		hand, &env->player.weapon.weapon_0.width,
		&env->player.weapon.weapon_0.height);
	free(hand);
	hand = ft_strjoin_2(env->parse.texture.hand, "/weapon_1.xpm");
	env->player.weapon.weapon_1.img = mlx_xpm_file_to_image(env->window.mlx,
		hand, &env->player.weapon.weapon_1.width,
		&env->player.weapon.weapon_1.height);
	free(hand);
	hand = ft_strjoin_2(env->parse.texture.hand, "/weapon_2.xpm");
	env->player.weapon.weapon_2.img = mlx_xpm_file_to_image(env->window.mlx,
		hand, &env->player.weapon.weapon_2.width,
		&env->player.weapon.weapon_2.height);
	free(hand);
	hand = ft_strjoin_2(env->parse.texture.hand, "/weapon_3.xpm");
	env->player.weapon.weapon_3.img = mlx_xpm_file_to_image(env->window.mlx,
		hand, &env->player.weapon.weapon_3.width,
		&env->player.weapon.weapon_3.height);
	free(hand);
	get_hand_adress_2(env);
}

static void		clean_sprite(t_env_cub3d *env)
{
	int	i;

	i = 0;
	env->current_sprite = 0;
	while (i < MAX_SPRITE)
	{
		env->sprite[i].size = 0;
		env->sprite[i].distance = 0.0;
		if (!(env->sprite[i].height = malloc(env->parse.resolution.x
			* (sizeof(int)))))
			ft_error("Malloc error", 1);
		env->sprite[i].if_see = 0;
		env->sprite[i].is_shooting = FALSE;
		env->sprite[i++].frame = 0;
	}
}

void			initialize_draw(t_env_cub3d *env)
{
	int x;
	int	y;

	x = env->parse.resolution.x;
	y = env->parse.resolution.y;
	env->player.plane.angle_ray = FOV / x;
	env->player.plane.center_x = x / 2.0;
	env->player.plane.center_y = (y - (y / 6)) / 2.0;
	env->player.plane.head_position = env->player.plane.center_y;
	env->player.plane.dimension_plane = x * (y - (y / 6));
	env->player.plane.z_position = 0.5;
	env->player.plane.distance_plane = (x / 2.0) / tan((FOV / 2) * 0.01745329);
	env->player.height = 0.5;
	env->player.map_case = '0';
	env->player.life.life = 100;
	env->player.life.armor = 50;
	env->player.is_end_level = FALSE;
	env->screen_height = (!env->parse.is_hud) ? (env->parse.resolution.y) :
											((env->player.plane.center_y) * 2);
	env->current_height = (env->player.height - 0.5);
	env->ray.orig_x = env->player.pos.x;
	env->ray.orig_y = env->player.pos.y;
	env->ray.found_sprite = FALSE;
	clean_sprite(env);
}
