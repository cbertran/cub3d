/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cub3d.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cbertran <cbertran@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/15 15:33:30 by cbertran          #+#    #+#             */
/*   Updated: 2020/06/07 19:36:26 by cbertran         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CUB3D_H
# define CUB3D_H
# include <math.h>
# include <fcntl.h>
# include <limits.h>
# include <unistd.h>
# include <stdlib.h>
# include <stdio.h>
# include <string.h>
# include <errno.h>
# include <time.h>
# include <X11/X.h>
# include "mlx.h"
# include <SDL/SDL.h>
# include <SDL/SDL_mixer.h>
# include "key.h"
# include "player.h"
# include "get_next_line.h"
# include "function.h"
# include "struct.h"
# include "my_mlx.h"
# define TRUE 1
# define FALSE 0
# define DEG_TO_RAD 0.017453292519943
# define M_PI 3.14159265358979323846
# define M_2PI 6.28318530717958647692
# define M_PI_2 1.57079632679489661923

void	mlx_initialize(t_env_cub3d *env, char *s);
void	mlx_create_image(t_env_cub3d *env, t_image *img);
void	my_mlx_pixel_put(t_image *data, int x, int y, int color);
void	mlx_draw_rectangle(t_env_cub3d *e, t_image *d, t_rectangle *r);
void	mlx_draw_circle(t_env_cub3d *e, t_image *d, t_circle *r);
void	mlx_draw_line(t_env_cub3d *e, t_image *d, t_line *l);

void	initialize_draw(t_env_cub3d *env);
void	launch_game(t_env_cub3d *env, int start);
void	draw_hud(t_env_cub3d *env);
void	ft_error(char *str, int error_code);
void	check_link(t_env_cub3d *env, int argc, char **argv);
void	initialize_parse(t_env_cub3d *parse, char *file);
int		check_start_parse(t_env_cub3d *var, char *line, char *sch);
int		is_present(char s, char *search);
int		is_different(char *s, char search);
void	sum_check(t_check_parse *var);
void	check_after_argument(char *line, int i);
void	check_after_ok_sum(char *line);
void	check_parse_sum_sprite(t_env_cub3d *env);
void	reset_fill_parse(t_env_cub3d *env);
void	fill_resolution(t_env_cub3d *env, const char *s, t_resolution *var,
							int y);
void	fill_path(t_env_cub3d *parse, char *s, t_texture *var, int type);
void	fill_sprite_struct(t_env_cub3d *var, int x, int y);
void	fill_height_wall(char *s, t_env_cub3d *var);
int		get_wall(t_env_cub3d *var, char c);
void	check_texture(char *file);
void	fill_path_sprite(t_env_cub3d *env, char *s, t_texture *var, int x);
void	fill_path_bonus(char *s, t_color *color, t_texture *var, int type);
void	bonus_sprite(t_env_cub3d *var, int x, char *s, char type);
void	fill_path_bonus_other(char *s, char *var, int x);
void	is_color(t_env_cub3d *env, char *s, int type);
void	fill_color(char *s, t_color *var, int x, int y);
void	sky_box(t_env_cub3d *env);
void	get_sprite_adress(t_env_cub3d *env);
void	get_skybox_adress(t_env_cub3d *env);
void	get_hand_adress(t_env_cub3d *env);
void	get_number_adress(t_env_cub3d *env, char *link);

void	malloc_map(t_env_cub3d *var);
void	check_map(t_env_cub3d *var, char *line);
void	fill_map(t_env_cub3d *var, char *file);
void	if_map_close(t_env_cub3d *var);
void	is_escape(t_env_cub3d *var, int x, int y);
void	if_player_escape(t_env_cub3d *var);
void	is_correct_hole(t_env_cub3d *var, int x, int y);

int		calc_color(t_color *color);
void	open_texture(t_env_cub3d *env, char *s);
void	free_memory(t_env_cub3d *var);

int		mlx_event(t_env_cub3d *var);
int		render_next_frame(t_env_cub3d *env);
int		key_release(int key, t_env_cub3d *env);
int		key_press(int key, t_env_cub3d *env);
int		close_windows(int key, t_env_cub3d *env);
int		reduce_windows(t_env_cub3d *env);
int		close_windows_cross(t_env_cub3d *env);
void	move_player(t_env_cub3d *env, t_image *img);

void	initialize_sound_engine(t_env_cub3d *env);
void	play_music(t_env_cub3d *env);
void	play_sound(t_env_cub3d *env, int channel, Mix_Chunk *chunk);
void	close_sound_engine(t_env_cub3d *env);

void	special_movement(t_env_cub3d *env);

void	raycasting(t_env_cub3d *env);
void	calc_distance_with_wall(t_env_cub3d *env);
double	calc_wall(t_env_cub3d *env, double current_height);
void	draw_wall(t_env_cub3d *env, double current_height, int reset_i);
void	draw_number(t_env_cub3d *env, t_screen_number *var, t_number *number,
						t_file_tex *tex);

void	screen_number(t_env_cub3d *env);
void	screen_life(t_env_cub3d *env);

void	if_detect(t_env_cub3d *env);
void	remove_fisheye(t_env_cub3d *env);
int		ck_le(char c);
int		color_from_image(t_file_tex *img, int x, int y);
void	draw_texture(t_env_cub3d *env, t_file_tex *tex, int column, int i);
void	texture_orientation(t_env_cub3d *env, int column, int i);
double	get_player_height(t_env_cub3d *env, double x, double y, int is_dif);
void	found_max_height(t_env_cub3d *env, double *max_height);
void	hand(t_env_cub3d *env, t_file_tex *weapon);

void	ft_sort_sprite(t_sprite *tab, int length);
void	calc_texture_value(t_env_cub3d *env, t_file_tex *tex);
void	calc_dimension_sprite(t_env_cub3d *env, int i);
void	calc_texture_value_1(t_env_cub3d *env, t_file_tex *tex);
void	calc_texture_value_2(t_env_cub3d *env, t_file_tex *tex, int line);
void	check_is_shoot_value(t_env_cub3d *env);
void	if_sprite(t_env_cub3d *env, t_file_tex *tex);
void	calc_distance_and_sort_sprite(t_env_cub3d *env);
void	move_sprite(t_env_cub3d *env, t_sprite *sprite);
void	clean_sprite_struct(t_env_cub3d *env, int i);
void	death_sprite(t_env_cub3d *env, int id, int i);
void	fire_sprite(t_env_cub3d *env, int id, int i);
void	moving_sprite(t_env_cub3d *env, int id, int i);
void	draw_sprite(t_env_cub3d *env);

void	is_shoot(t_env_cub3d *env);
void	death_player(t_env_cub3d *env);
void	bonus(t_env_cub3d *env);
void	take_screenshot(t_env_cub3d *env, t_image *img);

#endif
