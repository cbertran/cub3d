/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   player.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cbertran <cbertran@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/29 17:34:48 by cbertran          #+#    #+#             */
/*   Updated: 2020/05/31 13:44:12 by cbertran         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_PLAYER_H
# define FT_PLAYER_H
# define KEY_NUMBER 68000
# include "my_mlx.h"

typedef struct	s_position
{
	float		x;
	float		y;
	float		angle;
}				t_position;

typedef struct	s_direction
{
	float		x;
	float		y;
}				t_direction;

typedef struct	s_plane
{
	double		angle_ray;
	float		dimension_plane;
	float		center_x;
	float		center_y;
	float		distance_plane;
	float		head_position;
	float		z_position;
}				t_plane;

typedef struct	s_weapon
{
	t_file_tex	weapon_0;
	t_file_tex	weapon_1;
	t_file_tex	weapon_2;
	t_file_tex	weapon_3;
	int			current_img;
	int			is_shoot;
	int			ammunition;
	int			id_sprite_shoot;
}				t_weapon;

typedef struct	s_life
{
	int			life;
	int			armor;
}				t_life;

typedef struct	s_num_tex
{
	t_file_tex	zero;
	t_file_tex	one;
	t_file_tex	two;
	t_file_tex	three;
	t_file_tex	four;
	t_file_tex	five;
	t_file_tex	six;
	t_file_tex	seven;
	t_file_tex	eight;
	t_file_tex	nine;
	t_file_tex	health;
	t_file_tex	finish;
}				t_num_tex;

typedef struct	s_player
{
	t_position	pos;
	t_direction	dir;
	t_plane		plane;
	t_life		life;
	t_weapon	weapon;
	t_num_tex	number;
	double		height;
	double		difference;
	int			key[KEY_NUMBER];
	int			is_crouch;
	int			is_jump;
	int			is_end_level;
	int			x_start;
	int			y_start;
	char		map_case;
	char		start_case;
}				t_player;

#endif
