/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   key.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cbertran <cbertran@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/02 17:48:27 by cbertran          #+#    #+#             */
/*   Updated: 2020/05/03 02:09:18 by cbertran         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef KEY_H
# define KEY_H

# define UP 122
# define DOWN 115
# define LEFT 113
# define RIGHT 100
# define TURN_LEFT 65361
# define TURN_RIGHT 65363
# define SHIFT_LEFT 65505
# define SHIFT_RIGHT 65506
# define LOOK_UP 65362
# define LOOK_DOWN 65364
# define CROUCH 99
# define JUMP 32
# define SHOOT 65507
# define SCREENSHOT 65481
# define QUIT 65307
# define SPEED 1
# define FOV 60.0

#endif
