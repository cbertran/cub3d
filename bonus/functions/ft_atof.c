/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atof.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cbertran <cbertran@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/04/10 17:25:00 by cbertran          #+#    #+#             */
/*   Updated: 2020/04/13 23:15:00 by cbertran         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

static int		ft_isspace(char c)
{
	if (c == ' ' || c == '\t' || c == '\n' || c == '\v' || c == '\f'
		|| c == '\r')
		return (1);
	return (0);
}

static double	ft_power(int x, int power)
{
	double	result;
	double	copy_power;

	result = x;
	copy_power = x;
	while (power++)
		result /= copy_power;
	return (result);
}

static double	is_mentissa(char *s, int i, int size, long dest)
{
	double	multiple;

	while (s[i] >= '0' && s[i] <= '9')
	{
		dest = (dest * 10) + (s[i] - '0');
		size--;
		i++;
	}
	multiple = ft_power(10.0, --size);
	return (dest * multiple);
}

double			ft_atof(char *s)
{
	int		size;
	int		negative;
	int		i;
	long	dest;
	double	end_var;

	dest = 0;
	i = 0;
	size = 0;
	end_var = 0.0;
	negative = 1;
	while (ft_isspace(s[i]))
		i++;
	if (s[i] == '-' || s[i] == '+')
		negative = (s[i++] == '-') ? -1 : 1;
	while (s[i] >= '0' && s[i] <= '9')
	{
		dest = (dest * 10) + (s[i] - '0');
		i++;
	}
	if (s[i] == '.')
		end_var = is_mentissa(s, ++i, size, dest);
	else
		end_var += dest;
	return (end_var * negative);
}
