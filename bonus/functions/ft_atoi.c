/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cbertran <cbertran@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/08 19:22:23 by cbertran          #+#    #+#             */
/*   Updated: 2020/05/29 00:38:59 by cbertran         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "function.h"

static int	ft_isspace(char c)
{
	if (c == ' ' || c == '\t' || c == '\n' || c == '\v'
		|| c == '\f' || c == '\r')
		return (1);
	return (0);
}

int			ft_atoi(const char *str)
{
	int	dest;
	int	negative;
	int	i;

	dest = 0;
	negative = 0;
	i = 0;
	while (ft_isspace(str[i]))
		i++;
	if (str[i] == '-' || str[i] == '+')
	{
		if (str[i] == '-')
			negative = -1;
		i++;
	}
	while (str[i] >= '0' && str[i] <= '9')
	{
		dest = (dest * 10) + (str[i] - '0');
		i++;
	}
	if (negative == -1)
		return (dest * negative);
	else
		return (dest);
}

char		*ft_itoa(int n)
{
	char	buff[12];
	int		i;
	int		sign;

	i = 10;
	buff[11] = 0;
	sign = n < 0 ? 1 : 0;
	buff[10] = '0';
	if (!n)
		--i;
	while (n)
	{
		buff[i--] = ((n % 10) < 0 ? -(n % 10) : (n % 10)) + '0';
		n /= 10;
	}
	if (sign)
		buff[i--] = '-';
	return (ft_strdup(buff + i + 1));
}

static int	ft_strlen(const char *s)
{
	unsigned long int	len;

	len = 0;
	while (*s++)
		len++;
	return (len);
}

char		*ft_strdup(const char *s1)
{
	unsigned char	*dest;
	unsigned int	i;

	dest = (unsigned char *)s1;
	i = 0;
	if (!(dest = (unsigned char *)malloc(ft_strlen(s1) + 1)))
		return (0);
	while (*s1)
		dest[i++] = *s1++;
	dest[i] = '\0';
	return ((char *)dest);
}
