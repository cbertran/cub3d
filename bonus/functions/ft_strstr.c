/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cbertran <cbertran@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/08 18:53:05 by cbertran          #+#    #+#             */
/*   Updated: 2020/01/25 18:58:31 by cbertran         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "function.h"

char	*ft_strstr(char *str, char *to_find)
{
	unsigned int	i;
	unsigned int	found;

	i = 0;
	found = 0;
	if (to_find[0] == '\0')
		return (str);
	while (str[i] != '\0')
	{
		found = 0;
		if (str[i] == to_find[found])
		{
			found = 0;
			while (str[i] == to_find[found])
			{
				found++;
				i++;
			}
			if (to_find[found] == '\0')
				return (&str[i - found]);
		}
		i = i - found;
		i++;
	}
	return (0);
}
