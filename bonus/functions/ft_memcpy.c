/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cbertran <cbertran@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/16 22:00:28 by cbertran          #+#    #+#             */
/*   Updated: 2020/01/25 18:58:21 by cbertran         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "function.h"

void	*ft_memcpy(void *dst, const void *src, size_t n)
{
	unsigned int	i;
	unsigned char	*first;
	unsigned char	*second;

	i = 0;
	first = (unsigned char *)dst;
	second = (unsigned char *)src;
	if (dst == NULL && src == NULL && n > 0)
		return (dst);
	while (i < n)
	{
		first[i] = second[i];
		i++;
	}
	return (dst);
}
