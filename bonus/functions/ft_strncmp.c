/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncmp.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cbertran <cbertran@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/08 17:14:48 by cbertran          #+#    #+#             */
/*   Updated: 2020/01/25 14:43:40 by cbertran         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "function.h"

int		ft_strncmp(const char *s1, const char *s2, size_t n)
{
	unsigned char	first;
	unsigned char	second;

	while (n-- > 0)
	{
		first = (unsigned char)*s1++;
		second = (unsigned char)*s2++;
		if (first == '\0' || first != second)
			return (first - second);
	}
	return (first - second);
}
