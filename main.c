/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cbertran <cbertran@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/15 15:31:21 by cbertran          #+#    #+#             */
/*   Updated: 2020/06/05 14:56:41 by cbertran         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

void		launch_game(t_env_cub3d *env, int start)
{
	initialize_parse(env, env->argv);
	if (start == 1)
		mlx_initialize(env, "Cub3D");
	open_texture(env, env->argv);
	initialize_draw(env);
}

static void	screenshoot_arg(t_env_cub3d *env)
{
	int	i;

	i = 0;
	while (i < env->nbr_sprite)
	{
		env->sprite[i].if_see = 0;
		env->sprite[i++].is_shoot = FALSE;
	}
	mlx_create_image(env, &env->game);
	sky_box(env);
	raycasting(env);
	if (env->parse.is_hud)
	{
		draw_hud(env);
		move_player(env, &env->game);
	}
	bonus(env);
	take_screenshot(env, &env->game);
	free_memory(env);
	exit(EXIT_SUCCESS);
}

int			main(int argc, char **argv)
{
	t_env_cub3d	env;

	check_link(&env, argc, argv);
	env.argv = argv[1];
	launch_game(&env, 1);
	if (env.is_bmp == TRUE)
		screenshoot_arg(&env);
	mlx_hook(env.window.window, 2, (1L << 0), key_press, &env);
	mlx_hook(env.window.window, 12, (1L << 17), reduce_windows, &env);
	mlx_hook(env.window.window, 17, (1L << 17), close_windows_cross, &env);
	mlx_hook(env.window.window, 3, (1L << 1), key_release, &env);
	mlx_loop_hook(env.window.mlx, render_next_frame, &env);
	mlx_loop(env.window.mlx);
	return (0);
}
