/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_rand.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cbertran <cbertran@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/25 19:35:05 by cbertran          #+#    #+#             */
/*   Updated: 2020/05/29 00:40:28 by cbertran         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "function.h"

int	ft_rand(unsigned int seed)
{
	seed = (1103515245 * seed) % 2147483647;
	return (seed);
}

int	ft_rrand(unsigned int seed, int limit)
{
	int	divisor;
	int	retval;

	divisor = 2147483647 / (limit + 1);
	retval = ft_rand(seed) / divisor;
	while (retval > limit)
		retval = ft_rand(seed) / divisor;
	return (retval);
}
