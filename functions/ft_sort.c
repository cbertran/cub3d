/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sort.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cbertran <cbertran@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/10 16:57:45 by cbertran          #+#    #+#             */
/*   Updated: 2020/03/10 18:27:07 by cbertran         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "function.h"

static void	fill_save_tab(t_sort *var, int *tab, int end_one, int start_one)
{
	while (var->i_save <= end_one)
	{
		var->save_tab[var->i_save - start_one] = tab[var->i_save];
		var->i_save++;
	}
}

static void	fusion(int *tab, int start_one, int end_one, int end_two)
{
	t_sort	var;

	var.count_one = start_one;
	var.count_two = end_one + 1;
	if (!(var.save_tab = malloc((end_one - start_one + 1) * sizeof(int))))
		return ;
	var.i = start_one;
	var.i_save = start_one;
	fill_save_tab(&var, tab, end_one, start_one);
	while (var.i <= end_two)
	{
		if (var.count_one == (end_one + 1))
			break ;
		else if (var.count_two == (end_two + 1))
			tab[var.i] = var.save_tab[var.count_one++ - start_one];
		else if (var.save_tab[var.count_one - start_one] > tab[var.count_two])
			tab[var.i] = var.save_tab[var.count_one++ - start_one];
		else
			tab[var.i] = tab[var.count_two++];
		var.i++;
	}
	free(var.save_tab);
}

static void	launch_sort(int *tab, int start, int end)
{
	int	middle;

	if (start != end)
	{
		middle = (end + start) / 2;
		launch_sort(tab, start, middle);
		launch_sort(tab, middle + 1, end);
		fusion(tab, start, middle, end);
	}
}

void		ft_sort(int *tab, int length)
{
	if (length > 0)
		launch_sort(tab, 0, length - 1);
}
