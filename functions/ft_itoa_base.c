/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa_base.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cbertran <cbertran@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/01 17:51:46 by cbertran          #+#    #+#             */
/*   Updated: 2020/02/01 17:53:56 by cbertran         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "function.h"

static size_t	ft_strlen(const char *s)
{
	unsigned long int	len;

	len = 0;
	while (*s++)
		len++;
	return (len);
}

static char		*ft_swap(char *str, int negative)
{
	char	*tmp;
	int		start;
	int		end;
	int		i;

	start = 0;
	i = ft_strlen(str);
	end = i - 1;
	if (!(tmp = malloc(sizeof(char) * i + 1)))
		return (NULL);
	if (negative == 1)
		tmp[start++] = '-';
	while (start <= i)
		tmp[start++] = str[end--];
	tmp[start - 1] = '\0';
	return (tmp);
}

static int		ft_negative(long int *number)
{
	int			negative;
	long int	minus;

	negative = 1;
	minus = -1;
	*number *= minus;
	return (negative);
}

char			*ft_itoa_base(int number, int base, int upper)
{
	char			str[12];
	long int		value;
	int				negative;
	int				rem;
	unsigned int	i;

	value = (long int)number;
	i = 0;
	if (value < 0)
		negative = ft_negative(&value);
	else
		negative = 0;
	while (value != 0)
	{
		rem = value % base;
		if (rem < 10)
			rem += 48;
		else
			rem += (upper > 0 ? 55 : 87);
		str[i++] = rem;
		value /= base;
	}
	str[i] = '\0';
	return (ft_swap(str, negative));
}
