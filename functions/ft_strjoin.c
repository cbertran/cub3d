/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strjoin.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cbertran <cbertran@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/03 23:15:17 by cbertran          #+#    #+#             */
/*   Updated: 2020/05/03 23:20:08 by cbertran         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "function.h"

char	*ft_strjoin_2(char const *s1, char const *s2)
{
	unsigned int	width_s1;
	unsigned int	width_s2;
	unsigned int	i;
	char			*dest;

	if (s1 == NULL || s2 == NULL)
		return (NULL);
	i = 0;
	while (s1[i] != '\0')
		i++;
	width_s1 = i;
	i = 0;
	while (s2[i] != '\0')
		i++;
	width_s2 = i;
	i = 0;
	if (!(dest = malloc(sizeof(char) * (width_s1 + width_s2 + 1))))
		return (NULL);
	while (*s1)
		dest[i++] = *s1++;
	while (*s2)
		dest[i++] = *s2++;
	dest[i] = '\0';
	return (dest);
}
