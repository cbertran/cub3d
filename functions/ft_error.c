/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_error.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cbertran <cbertran@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/20 17:06:40 by cbertran          #+#    #+#             */
/*   Updated: 2020/06/04 19:06:08 by cbertran         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

/*
**	Error code :
**	1	- Empty map or too many arguments
**	2	- Wrong extension for map
**	3	- Not same number of character on map
**	4	- Error with malloc
**	5	- Wrong dimension
**	6	- Wrong path
**	7	- Wrong color
**	8	- Ambiguous player placement
**	9	- The file does not exist
**	10	- One of the textures does not exist
**	11	- Missing texture definition | Duplication of textures
**	12	- Missing resolution definition
**	13	- Missing floor color definition
**	14	- Missing ceilling color definition
**	15	- Fail to open texture(s)
**	16	- Fail to create bmp
**	17	- Fail with mlx
**	18	- Error width sprites
**	19	- Fail create sound
**	20	- Forbidden character
*/

void	ft_error(char *str, int error_code)
{
	ft_putstr("Error\n");
	ft_putstr(str);
	ft_putchar('\n');
	ft_putstr(">> error code : ");
	ft_putnbr(error_code);
	ft_putchar('\n');
	exit(error_code);
}
