/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   special_movement.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cbertran <cbertran@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/18 20:19:13 by cbertran          #+#    #+#             */
/*   Updated: 2020/05/31 19:23:01 by cbertran         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

static	void	look_up_and_down(t_env_cub3d *env)
{
	if (env->player.key[LOOK_UP])
		env->player.plane.head_position = env->player.plane.center_y
											+ (env->player.plane.center_y / 2);
	else if (env->player.key[LOOK_DOWN])
		env->player.plane.head_position = env->player.plane.center_y
											- (env->player.plane.center_y / 2);
	else
		env->player.plane.head_position = env->player.plane.center_y;
}

static void		crouch(t_env_cub3d *env, float *step, int *is_crouch,
									int *boolean)
{
	if (env->player.plane.z_position > (env->player.height - 0.3)
		&& *boolean == 0)
		env->player.plane.z_position -= *step;
	else if (env->player.plane.z_position <= (env->player.height - 0.3)
		&& *boolean == 0)
		*boolean = 1;
	if (!env->player.key[CROUCH])
	{
		if (env->player.plane.z_position < env->player.height && *boolean == 1)
			env->player.plane.z_position += *step;
		else if ((env->player.plane.z_position) >= env->player.height
			&& *boolean == 1)
		{
			env->player.plane.z_position = env->player.height;
			*boolean = 0;
			*is_crouch = 0;
		}
	}
}

static void		jump(t_env_cub3d *env, float *step, int *is_jump,
									int *boolean)
{
	if (env->player.plane.z_position <= (env->player.height + 0.4)
		&& *boolean == 0)
		env->player.plane.z_position += *step;
	else if (env->player.plane.z_position >= (env->player.height + 0.4)
		&& *boolean == 0)
		*boolean = 1;
	if (env->player.plane.z_position >= env->player.height && *boolean == 1)
		env->player.plane.z_position -= *step;
	else if (env->player.plane.z_position <= env->player.height
		&& *boolean == 1)
	{
		env->player.plane.z_position = env->player.height;
		*boolean = 0;
		*is_jump = 0;
	}
}

static void		crouch_and_jump(t_env_cub3d *env)
{
	static float	step;
	static int		boolean;

	(!boolean) ? (boolean = 0) : 0;
	(!env->player.is_crouch) ? (env->player.is_crouch = 0) : 0;
	(!env->player.is_jump) ? (env->player.is_jump = 0) : 0;
	(!step) ? (step = 0.037) : 0;
	if (env->player.is_jump == 0 && env->player.key[CROUCH])
		env->player.is_crouch = 1;
	else if (env->player.is_crouch == 0 && env->player.key[JUMP])
		env->player.is_jump = 1;
	if (env->player.is_crouch == 1)
	{
		crouch(env, &step, &env->player.is_crouch, &boolean);
		env->player.plane.z_position += get_player_height(env,
								env->player.pos.x, env->player.pos.y, TRUE);
	}
	else if (env->player.is_jump == 1)
	{
		jump(env, &step, &env->player.is_jump, &boolean);
		env->player.plane.z_position += get_player_height(env,
								env->player.pos.x, env->player.pos.y, TRUE);
	}
	else if (!env->player.is_crouch && !env->player.is_jump)
		env->player.plane.z_position = env->player.height;
}

void			special_movement(t_env_cub3d *env)
{
	int	x;

	x = 0;
	if (is_present(env->player.map_case, "NSEW"))
		env->player.difference = 0.0;
	else if (is_present(env->player.map_case, "23456789ABCDFGHIJKLMOPQRTUVXYZ"))
	{
		while (env->sprite[x].type != '0' && x < env->nbr_sprite)
		{
			if (env->sprite[x].x_case == (int)(env->player.pos.x)
				&& env->sprite[x].y_case == (int)(env->player.pos.y))
			{
				env->player.difference = env->wall[get_wall(env,
								env->sprite[x].map_case)].height;
				break ;
			}
			x++;
		}
	}
	else
		env->player.difference = env->wall[get_wall(env,
									env->player.map_case)].height;
	env->player.height = 0.5 + env->player.difference;
	(env->parse.is_hud) ? (look_up_and_down(env)) : 0;
	(env->parse.is_hud) ? (crouch_and_jump(env)) : 0;
}
