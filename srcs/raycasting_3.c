/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   raycasting_3.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cbertran <cbertran@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/04/19 15:24:20 by cbertran          #+#    #+#             */
/*   Updated: 2020/05/31 02:55:41 by cbertran         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

static void	calc_position_texture(t_env_cub3d *env, t_file_tex *texture,
									double height_wall)
{
	if (env->ray.side == 0)
		texture->wall_hit_x = (env->player.pos.y + env->ray.distance_for_texture
								* env->ray.dir_y);
	else
		texture->wall_hit_x = (env->player.pos.x + env->ray.distance_for_texture
								* env->ray.dir_x);
	texture->wall_hit_x -= floor(texture->wall_hit_x);
	texture->tex_x = (int)(texture->wall_hit_x * (double)texture->width);
	if ((env->ray.side == 0 && env->ray.dir_x > 0)
		|| (env->ray.side == 1 && env->ray.dir_y < 0))
		texture->tex_x = texture->width - texture->tex_x - 1;
	texture->step = 1.0 * texture->height / (env->ray.wall.height
						/ height_wall);
	texture->tex_pos = (env->ray.wall.start - env->ray.wall.end
						+ env->ray.wall.height) * texture->step;
	texture->tex_y = env->ray.wall.end;
}

/*
**	Order:
**	North - South - West - East
*/

static void	jump_to_correct_pixels(t_env_cub3d *env, int i)
{
	int	x;

	x = env->ray.wall.end - i;
	if (env->ray.side == 0 && (ck_le(env->ray.map_case) == 1))
	{
		if (env->ray.step_x < 0)
			env->texture.north.tex_pos += (env->texture.north.step * x);
		else
			env->texture.south.tex_pos += (env->texture.south.step * x);
	}
	else if (ck_le(env->ray.map_case) == 1)
	{
		if (env->ray.step_y < 0)
			env->texture.west.tex_pos += (env->texture.west.step * x);
		else
			env->texture.east.tex_pos += (env->texture.east.step * x);
	}
}

static void	if_texture_appear(t_env_cub3d *env, double height_wall,
								int stop_process)
{
	if (env->ray.side == 0 && (ck_le(env->ray.map_case) == 1))
	{
		if (env->ray.step_x < 0)
			calc_position_texture(env, &env->texture.north, height_wall);
		else
			calc_position_texture(env, &env->texture.south, height_wall);
	}
	else if (ck_le(env->ray.map_case) == 1)
	{
		if (env->ray.step_y < 0)
			calc_position_texture(env, &env->texture.west, height_wall);
		else
			calc_position_texture(env, &env->texture.east, height_wall);
	}
	if (env->parse.texture.floor[0] == '.')
		calc_position_texture(env, &env->texture.floor, height_wall);
	if (env->parse.texture.ceilling[0] == '.')
		calc_position_texture(env, &env->texture.ceilling, height_wall);
	if (env->ray.wall.end > env->ray.i && stop_process == FALSE)
		jump_to_correct_pixels(env, env->ray.i);
}

static void	while_draw_wall(t_env_cub3d *env, int stop_process)
{
	while (env->ray.i >= 0 && stop_process == FALSE)
	{
		if (env->ray.i >= env->ray.wall.end)
			my_mlx_pixel_put(&env->game, env->ray.column, env->ray.i,
				env->parse.floor.hexa);
		if (env->ray.i < env->ray.wall.end
			&& env->ray.i > env->ray.wall.start)
			texture_orientation(env, env->ray.column, env->ray.i);
		else if (env->ray.i <= env->ray.wall.start)
			break ;
		env->ray.i--;
	}
}

void		draw_wall(t_env_cub3d *env, double current_height, int reset_i)
{
	static int		stop_process;

	(!env->ray.i) ? (env->ray.i = env->screen_height) : 0;
	(!stop_process) ? (stop_process = FALSE) : 0;
	if (reset_i == FALSE)
	{
		if (stop_process == FALSE)
			if_texture_appear(env, (calc_wall(env, current_height)),
								stop_process);
		while_draw_wall(env, stop_process);
		(env->ray.wall.start <= 0) ? (stop_process = TRUE) : 0;
	}
	else if (reset_i == TRUE)
	{
		env->ray.i = env->screen_height;
		stop_process = FALSE;
	}
}
