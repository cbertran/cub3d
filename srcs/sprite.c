/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sprite.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cbertran <cbertran@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/16 21:07:11 by cbertran          #+#    #+#             */
/*   Updated: 2020/05/31 14:56:56 by cbertran         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

static void	move_sprite_2(t_env_cub3d *env, t_sprite *sprite, int x, int y)
{
	double	difference;
	int		z;

	z = 0;
	while (env->is_hit[z] != '#' && z < 63)
	{
		if (env->is_hit[z] == env->map[x][y] && sprite->type != env->is_hit[z])
			return ;
		z++;
	}
	if (x == (int)env->player.pos.x && y == (int)env->player.pos.y)
		return ;
	difference = sprite->futur_height - sprite->z;
	if ((sprite->futur_height > sprite->z && difference <= 0.25)
		|| (sprite->futur_height <= sprite->z))
	{
		sprite->x -= cos(sprite->angle * DEG_TO_RAD) * 0.05;
		sprite->y -= sin(sprite->angle * DEG_TO_RAD) * 0.05;
		sprite->z = sprite->futur_height;
	}
}

void		move_sprite(t_env_cub3d *env, t_sprite *sprite)
{
	int		x;
	int		y;
	int		z;

	x = sprite->x - cos(sprite->angle * DEG_TO_RAD) * 0.05;
	y = sprite->y - sin(sprite->angle * DEG_TO_RAD) * 0.05;
	if (is_present(env->map[x][y], "NSEW"))
		sprite->futur_height = 0.0;
	else if (is_present(env->map[x][y], "23456789ABCDFGHIJKLMOPQRTUVXYZ"))
	{
		z = 0;
		while (env->sprite[z].type != '0' && z < env->nbr_sprite)
		{
			if (env->sprite[z].x_case == x && env->sprite[z].y_case == y)
			{
				sprite->futur_height = env->sprite[z].z;
				break ;
			}
			z++;
		}
	}
	else
		sprite->futur_height = env->wall[get_wall(env, env->map[x][y])].height;
	move_sprite_2(env, sprite, x, y);
}

static void	is_hurt_sprite(t_env_cub3d *env, int id, int i)
{
	if (env->sprite[i].frame > 0)
	{
		env->sprite[i].health -= 4;
		if (env->sprite[i].health > 0)
		{
			env->sprite[i].frame = -9;
		}
	}
	env->sprite[i].link = &env->texture.sprite[id].shoot;
}

static void	texture_for_sprite(t_env_cub3d *env, int id, int i)
{
	if (env->sprite[i].is_shoot == TRUE && env->player.is_end_level == FALSE)
		is_hurt_sprite(env, id, i);
	if (env->sprite[i].is_shooting == FALSE)
		env->sprite[env->current_sprite].shooting = rand() % 50;
	if ((!env->sprite[i].shooting % 3 || env->sprite[i].is_shooting == TRUE)
		&& env->player.is_end_level == FALSE)
	{
		if (env->sprite[i].frame == -1)
			env->sprite[i].is_shooting = FALSE;
		else
			env->sprite[i].is_shooting = TRUE;
		fire_sprite(env, id, i);
	}
	if (env->sprite[i].health > 0)
		moving_sprite(env, id, i);
	else
		death_sprite(env, id, i);
	if (env->sprite[i].health > 0
		|| (env->sprite[i].health <= 0 && env->sprite[i].frame != -1))
		env->sprite[i].frame++;
}

void		draw_sprite(t_env_cub3d *env)
{
	int		id;
	int		i;

	calc_distance_and_sort_sprite(env);
	while (env->current_sprite < env->nbr_sprite)
	{
		calc_dimension_sprite(env, env->current_sprite);
		i = env->current_sprite;
		if (env->sprite[i].if_see && env->sprite[i].sprite_type != -1)
		{
			id = env->sprite[i].tab_adress;
			check_is_shoot_value(env);
			if (env->sprite[i].sprite_type == 1 && env->sprite[i].health > -100)
				texture_for_sprite(env, id, i);
			else if (env->sprite[i].sprite_type != 1)
				env->sprite[i].link = &env->texture.sprite[id].alone;
			while (env->column < env->sprite[i].end_x)
				calc_texture_value_1(env, env->sprite[i].link);
			if (env->sprite[i].sprite_type == 1 && env->sprite[i].health > -100
			&& !env->sprite[i].is_shoot && !env->sprite[i].is_shooting
			&& env->sprite[i].distance <= 6.0 && !env->player.is_end_level)
				move_sprite(env, &env->sprite[i]);
		}
		env->current_sprite++;
	}
}
