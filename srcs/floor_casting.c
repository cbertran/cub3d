/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   floor_casting.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cbertran <cbertran@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/19 18:01:43 by cbertran          #+#    #+#             */
/*   Updated: 2020/05/29 18:04:04 by cbertran         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

static void	color_sky(t_env_cub3d *env)
{
	t_rectangle sky;

	sky.border = 0;
	sky.color = env->parse.ceilling.hexa;
	sky.height = env->screen_height;
	sky.width = env->parse.resolution.x;
	sky.x = 0;
	sky.y = 0;
	mlx_draw_rectangle(env, &env->game, &sky);
}

static void	texture_sky(t_env_cub3d *env, t_file_tex *skybox, double angle)
{
	skybox->wall_hit_x = angle;
	skybox->wall_hit_x -= floor(skybox->wall_hit_x);
	skybox->tex_x = (int)(skybox->wall_hit_x * (double)skybox->width);
	if (skybox->wall_hit_x < 0)
		skybox->tex_x = skybox->width - skybox->tex_x - 1;
	skybox->step = 1.0 * skybox->height / env->parse.resolution.x;
	skybox->tex_pos = skybox->step;
	skybox->tex_y = 0;
}

static void	screen_texture_sky(t_env_cub3d *env, t_file_tex *tex, int cl, int x)
{
	int	col;

	tex->tex_y = (int)tex->tex_pos & (tex->height - 1);
	tex->tex_pos += tex->step;
	col = color_from_image(tex, tex->tex_x, tex->tex_y);
	my_mlx_pixel_put(&env->game, cl, x, col);
}

void		sky_box(t_env_cub3d *env)
{
	int		x;
	int		column;
	double	angle;

	if (env->parse.texture.skybox[0] == '.')
	{
		angle = env->player.pos.angle;
		column = 0;
		while (column < env->parse.resolution.x)
		{
			texture_sky(env, &env->texture.skybox, (angle * DEG_TO_RAD));
			angle -= env->player.plane.angle_ray;
			angle += (angle < 0) ? 360 : 0;
			x = 0;
			while (x < env->screen_height)
				screen_texture_sky(env, &env->texture.skybox, column, x++);
			column++;
		}
	}
	else
		color_sky(env);
}
