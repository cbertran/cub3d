/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   hand.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cbertran <cbertran@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/04/04 22:04:02 by cbertran          #+#    #+#             */
/*   Updated: 2020/05/10 18:31:37 by cbertran         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

static void	calc_d(t_env_cub3d *env, int x, int move, t_file_tex *weapon)
{
	int	d;

	d = (int)(x * 256 - ((((env->player.plane.center_y + move) * 2) * 128)
		+ env->hand.size * 128));
	weapon->tex_y = (int)(d * weapon->height / env->hand.size / 256);
}

static void	screen_hand(t_env_cub3d *env, int column, int x, t_file_tex *weapon)
{
	int	col;

	col = color_from_image(weapon, weapon->tex_x, weapon->tex_y);
	(col != 0x980088) ? my_mlx_pixel_put(&env->game, column, x, col) : 0;
}

static int	move_hand(t_env_cub3d *env)
{
	static int	move;
	static int	boolean;

	(!move) ? (move = 0) : 0;
	(!boolean) ? (boolean = 0) : 0;
	if (env->player.key[UP] || env->player.key[DOWN] || env->player.key[LEFT]
		|| env->player.key[RIGHT] || env->player.key[TURN_LEFT]
		|| env->player.key[TURN_RIGHT])
	{
		if (boolean == 0 && move < 20)
			move++;
		else if (move >= 20 && boolean == 0)
			boolean = 1;
		if (boolean == 1 && move >= 0)
			move--;
		else if (boolean == 1 && move <= 0)
			boolean = 0;
	}
	else
		move = 0;
	return (move);
}

void		hand(t_env_cub3d *env, t_file_tex *weapon)
{
	int			x;
	int			column;
	int			move;

	move = move_hand(env);
	env->hand.size = (int)(env->screen_height / 3 + 1);
	env->hand.start_y = (env->screen_height + move - env->hand.size);
	env->hand.end_y = env->screen_height + 1 + move;
	env->hand.start_x = env->player.plane.center_x - (env->hand.size / 2);
	env->hand.end_x = env->player.plane.center_x + (env->hand.size / 2) + move;
	column = env->hand.start_x;
	column += move;
	while (column < env->hand.end_x)
	{
		weapon->tex_x = (int)((256 * (column
			- (-env->hand.size / 2 + env->player.plane.center_x + move))
			* weapon->width / env->hand.size) / 256);
		x = env->hand.start_y;
		while (x < env->hand.end_y)
		{
			calc_d(env, x, move, weapon);
			screen_hand(env, column, x++, weapon);
		}
		column++;
	}
}
