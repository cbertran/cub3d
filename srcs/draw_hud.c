/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw_hud.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cbertran <cbertran@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/27 16:44:03 by cbertran          #+#    #+#             */
/*   Updated: 2020/05/31 19:06:36 by cbertran         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

static void	draw_mini_map(t_env_cub3d *env, t_image *img, t_rectangle *cube)
{
	int		y;
	int		x;
	int		save_y;

	y = 0;
	x = 0;
	save_y = cube->y;
	while (env->map[x])
	{
		y = 0;
		cube->x = env->parse.resolution.x - (env->parse.resolution.x / 6);
		while (env->map[x][y])
		{
			if (!(env->map[x][y] == '1' || env->map[x][y] == ' '))
				mlx_draw_rectangle(env, img, cube);
			cube->x += cube->width;
			cube->y = save_y;
			y++;
		}
		cube->y += cube->height;
		save_y = cube->y;
		x++;
	}
}

static void	mini_map(t_env_cub3d *env, t_image *img)
{
	t_rectangle	back;
	t_rectangle	cube;

	back.border = 0;
	back.width = env->parse.resolution.x / 6;
	back.height = env->parse.resolution.y / 6;
	back.x = env->parse.resolution.x - (env->parse.resolution.x / 6);
	back.y = env->parse.resolution.y - env->parse.resolution.y / 6;
	back.color = env->parse.minimap_back.hexa;
	cube.border = 0;
	cube.width = floor(env->parse.resolution.x / 6 / env->width_map.width);
	cube.height = floor(env->parse.resolution.y / 6 / env->width_map.line);
	cube.x = 0;
	cube.y = env->parse.resolution.y - (env->parse.resolution.y / 6);
	cube.color = env->parse.minimap_block.hexa;
	mlx_draw_rectangle(env, img, &back);
	draw_mini_map(env, img, &cube);
}

void		draw_hud(t_env_cub3d *env)
{
	t_rectangle	hud;

	hud.border = 0;
	hud.width = env->parse.resolution.x;
	hud.height = env->parse.resolution.y / 6;
	hud.x = 0;
	hud.y = env->parse.resolution.y - env->parse.resolution.y / 6;
	hud.color = env->parse.hud.hexa;
	mlx_draw_rectangle(env, &env->game, &hud);
	mini_map(env, &env->game);
	screen_number(env);
	screen_life(env);
}
