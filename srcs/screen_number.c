/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   screen_number.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cbertran <cbertran@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/09 18:49:18 by cbertran          #+#    #+#             */
/*   Updated: 2020/05/31 14:22:28 by cbertran         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

static void	print_number(t_env_cub3d *env, t_screen_number *var,
							t_file_tex *tex, int wyn)
{
	t_number	number;

	if (wyn == 100)
	{
		number.center_x = var->width / 2;
		number.center_y = (env->parse.resolution.y
							- (env->parse.resolution.y / 6)) + var->height / 2;
		number.x = number.center_x - (var->width / 2);
		number.y = number.center_y - (var->height / 2);
		number.x_end = number.center_x + (var->width / 2);
		number.y_end = number.center_y + (var->height / 2);
	}
	else if (wyn == 10)
	{
		number.x = var->width;
		number.y = (env->parse.resolution.y - (env->parse.resolution.y / 6));
		number.x_end = number.x + var->width;
		number.y_end = env->parse.resolution.y;
		number.center_x = number.x + var->width / 2;
		number.center_y = number.y + var->height / 2;
	}
	draw_number(env, var, &number, tex);
}

static void	unit(t_env_cub3d *env, t_screen_number *var, t_file_tex *tex)
{
	t_number	number;

	number.x = var->width * 2;
	number.y = (env->parse.resolution.y - (env->parse.resolution.y / 6));
	number.x_end = number.x + var->width;
	number.y_end = env->parse.resolution.y;
	number.center_x = number.x + var->width / 2;
	number.center_y = number.y + var->height / 2;
	draw_number(env, var, &number, tex);
}

t_file_tex	*choose_number(t_env_cub3d *env, int x)
{
	if (x == 1)
		return (&env->player.number.one);
	else if (x == 2)
		return (&env->player.number.two);
	else if (x == 3)
		return (&env->player.number.three);
	else if (x == 4)
		return (&env->player.number.four);
	else if (x == 5)
		return (&env->player.number.five);
	else if (x == 6)
		return (&env->player.number.six);
	else if (x == 7)
		return (&env->player.number.seven);
	else if (x == 8)
		return (&env->player.number.eight);
	else if (x == 9)
		return (&env->player.number.nine);
	return (&env->player.number.zero);
}

void		draw_number(t_env_cub3d *env, t_screen_number *var,
							t_number *number, t_file_tex *tex)
{
	while (number->x < number->x_end)
	{
		tex->tex_x = (int)((256 * (number->x - (-var->width / 2
						+ number->center_x)) * tex->width / var->width) / 256);
		number->y = number->center_y - (var->height / 2) + 2;
		while (number->y < number->y_end)
		{
			number->d = (int)(number->y * 256 - (((number->center_y * 2) * 128)
					+ var->height * 128));
			tex->tex_y = (int)(number->d * tex->height / var->height / 256);
			number->col = color_from_image(tex, tex->tex_x, tex->tex_y);
			(number->col != 0x980088) ? my_mlx_pixel_put(&env->game, number->x,
				number->y, number->col) : 0;
			number->y++;
		}
		number->x++;
	}
}

void		screen_number(t_env_cub3d *env)
{
	t_screen_number	var;
	int				num;

	var.height = env->parse.resolution.y / 6;
	var.width = env->parse.resolution.x / 10;
	var.number = ft_itoa(env->player.weapon.ammunition);
	num = (int)(env->player.weapon.ammunition / 100);
	print_number(env, &var, choose_number(env, num), 100);
	num = (int)(env->player.weapon.ammunition / 10);
	(num > 10) ? (num = num % 10) : 0;
	print_number(env, &var, choose_number(env, num), 10);
	num = (int)(env->player.weapon.ammunition);
	(num > 10) ? (num = num % 10) : 0;
	unit(env, &var, choose_number(env, num));
	free(var.number);
}
