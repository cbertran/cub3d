/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mlx_event.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cbertran <cbertran@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/28 18:53:22 by cbertran          #+#    #+#             */
/*   Updated: 2020/06/05 14:28:27 by cbertran         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

/*
**	Explanation of order and logical calc
**
**	 	   180
**  	    |
**   270 ---|--- 90
**  	    |
**			0
*/

static int	check_wall(t_env_cub3d *env, double *x, double *y, double speed)
{
	if (env->player.key[UP])
	{
		*x += cos(env->player.pos.angle * DEG_TO_RAD) * speed;
		*y += sin(env->player.pos.angle * DEG_TO_RAD) * speed;
	}
	if (env->player.key[DOWN])
	{
		*x -= cos(env->player.pos.angle * DEG_TO_RAD) * speed;
		*y -= sin(env->player.pos.angle * DEG_TO_RAD) * speed;
	}
	if (env->player.key[RIGHT])
	{
		*x += cos((env->player.pos.angle - 90) * DEG_TO_RAD) * speed;
		*y += sin((env->player.pos.angle - 90) * DEG_TO_RAD) * speed;
	}
	if (env->player.key[LEFT])
	{
		*x += cos((env->player.pos.angle + 90) * DEG_TO_RAD) * speed;
		*y += sin((env->player.pos.angle + 90) * DEG_TO_RAD) * speed;
	}
	if (get_player_height(env, *x, *y, TRUE) > 0.25
		|| env->map[(int)*x][(int)*y] == ' ')
		return (0);
	return (1);
}

static int	change_value_touch(t_env_cub3d *env)
{
	double			x;
	double			y;
	double			speed;

	speed = (env->player.key[SHIFT_LEFT] || env->player.key[SHIFT_RIGHT]) ?
		SPEED * 0.3 : SPEED * 0.1;
	if (env->player.key[TURN_LEFT])
		(env->player.key[SHIFT_LEFT] || env->player.key[SHIFT_RIGHT]) ?
			(env->player.pos.angle += 4) : (env->player.pos.angle += 2);
	if (env->player.key[TURN_RIGHT])
		(env->player.key[SHIFT_LEFT] || env->player.key[SHIFT_RIGHT]) ?
			(env->player.pos.angle -= 4) : (env->player.pos.angle -= 2);
	(env->player.pos.angle > 359) ? env->player.pos.angle -= 360 : 0;
	(env->player.pos.angle < 0) ? env->player.pos.angle += 360 : 0;
	x = env->player.pos.x;
	y = env->player.pos.y;
	env->player.map_case = env->map[(int)x][(int)y];
	if (check_wall(env, &x, &y, speed))
	{
		env->player.pos.x = x;
		env->player.pos.y = y;
	}
	return (0);
}

int			mlx_event(t_env_cub3d *env)
{
	if (env->player.key[QUIT])
		close_windows(env->player.key[QUIT], env);
	else if (env->player.life.life > 0)
	{
		if ((env->player.key[UP] || env->player.key[DOWN]
			|| env->player.key[LEFT] || env->player.key[RIGHT]
			|| env->player.key[TURN_LEFT] || env->player.key[TURN_RIGHT])
			&& env->player.is_end_level == FALSE)
			change_value_touch(env);
	}
	return (0);
}

int			render_next_frame(t_env_cub3d *env)
{
	int	i;

	i = 0;
	while (i < env->nbr_sprite)
	{
		env->sprite[i].if_see = 0;
		env->sprite[i++].is_shoot = FALSE;
	}
	mlx_create_image(env, &env->game);
	mlx_event(env);
	sky_box(env);
	raycasting(env);
	if (env->parse.is_hud)
	{
		draw_hud(env);
		move_player(env, &env->game);
	}
	bonus(env);
	if (env->player.key[SCREENSHOT])
		take_screenshot(env, &env->game);
	mlx_put_image_to_window(env->window.mlx, env->window.window,
								env->game.img, 0, 0);
	mlx_destroy_image(env->window.mlx, env->game.img);
	return (0);
}
