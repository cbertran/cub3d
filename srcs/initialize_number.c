/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   initialize_number.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cbertran <cbertran@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/10 19:33:51 by cbertran          #+#    #+#             */
/*   Updated: 2020/06/01 19:06:10 by cbertran         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

static void	number_import(t_env_cub3d *env, char *link, t_file_tex *tex)
{
	tex->img = mlx_xpm_file_to_image(env->window.mlx, link, &tex->width,
									&tex->height);
	if (tex->img == NULL)
		ft_error("Fail to open texture(s)", 15);
	tex->address = (int *)mlx_get_data_addr(tex->img, &tex->bpp,
						&tex->line_length, &tex->end);
}

static void	get_number_adress_2(t_env_cub3d *env, char *link)
{
	char	*s;

	s = ft_strjoin_2(link, "/textures/numbers/7.xpm");
	number_import(env, s, &env->player.number.seven);
	free(s);
	s = ft_strjoin_2(link, "/textures/numbers/8.xpm");
	number_import(env, s, &env->player.number.eight);
	free(s);
	s = ft_strjoin_2(link, "/textures/numbers/9.xpm");
	number_import(env, s, &env->player.number.nine);
	free(s);
	s = ft_strjoin_2(link, "/textures/numbers/health.xpm");
	number_import(env, s, &env->player.number.health);
	free(s);
	s = ft_strjoin_2(link, "/textures/numbers/finish.xpm");
	number_import(env, s, &env->player.number.finish);
	free(s);
}

void		get_number_adress(t_env_cub3d *env, char *link)
{
	char	*s;

	s = ft_strjoin_2(link, "/textures/numbers/0.xpm");
	number_import(env, s, &env->player.number.zero);
	free(s);
	s = ft_strjoin_2(link, "/textures/numbers/1.xpm");
	number_import(env, s, &env->player.number.one);
	free(s);
	s = ft_strjoin_2(link, "/textures/numbers/2.xpm");
	number_import(env, s, &env->player.number.two);
	free(s);
	s = ft_strjoin_2(link, "/textures/numbers/3.xpm");
	number_import(env, s, &env->player.number.three);
	free(s);
	s = ft_strjoin_2(link, "/textures/numbers/4.xpm");
	number_import(env, s, &env->player.number.four);
	free(s);
	s = ft_strjoin_2(link, "/textures/numbers/5.xpm");
	number_import(env, s, &env->player.number.five);
	free(s);
	s = ft_strjoin_2(link, "/textures/numbers/6.xpm");
	number_import(env, s, &env->player.number.six);
	free(s);
	get_number_adress_2(env, link);
}
