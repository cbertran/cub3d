/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   free.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cbertran <cbertran@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/22 19:29:31 by cbertran          #+#    #+#             */
/*   Updated: 2020/06/02 15:48:38 by cbertran         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

static void	free_number(t_env_cub3d *var)
{
	free(var->player.number.zero.img);
	free(var->player.number.one.img);
	free(var->player.number.two.img);
	free(var->player.number.three.img);
	free(var->player.number.four.img);
	free(var->player.number.five.img);
	free(var->player.number.six.img);
	free(var->player.number.seven.img);
	free(var->player.number.eight.img);
	free(var->player.number.nine.img);
	free(var->player.number.health.img);
	free(var->player.number.finish.img);
}

static void	free_bonus_and_texture(t_env_cub3d *var)
{
	free(var->texture.north.img);
	free(var->texture.south.img);
	free(var->texture.east.img);
	free(var->texture.west.img);
	if (var->parse.texture.skybox[0] == '.')
		free(var->texture.skybox.img);
	if (var->parse.texture.hand[0] == '.')
	{
		free(var->player.weapon.weapon_0.img);
		free(var->player.weapon.weapon_1.img);
		free(var->player.weapon.weapon_2.img);
		free(var->player.weapon.weapon_3.img);
	}
	if (var->parse.texture.floor[0] == '.')
		free(var->texture.floor.img);
	if (var->parse.texture.ceilling[0] == '.')
		free(var->texture.ceilling.img);
	if (var->parse.is_hud)
		free_number(var);
}

static void	free_animated_sprite(t_env_cub3d *var, int x)
{
	free(var->texture.sprite[x].death_1.img);
	free(var->texture.sprite[x].death_2.img);
	free(var->texture.sprite[x].death_3.img);
	free(var->texture.sprite[x].death_4.img);
	free(var->texture.sprite[x].death_5.img);
	free(var->texture.sprite[x].fire_1.img);
	free(var->texture.sprite[x].fire_2.img);
	free(var->texture.sprite[x].shoot.img);
	free(var->texture.sprite[x].walk_1.img);
	free(var->texture.sprite[x].walk_2.img);
	free(var->texture.sprite[x].walk_3.img);
	free(var->texture.sprite[x].walk_4.img);
}

void		free_memory(t_env_cub3d *var)
{
	int	x;

	x = var->width_map.line;
	while (x >= 0)
		free(var->map[x--]);
	free(var->map);
	x = 0;
	while (x < MAX_SPRITE)
		free(var->sprite[x++].height);
	x = 0;
	while (var->parse.id[x] != '0')
	{
		if (var->texture.sprite[x].type == 1)
			free_animated_sprite(var, x);
		else
			free(var->texture.sprite[x].alone.img);
		x++;
	}
	free_bonus_and_texture(var);
	mlx_destroy_image(var->window.mlx, var->game.img);
	free(var->window.mlx);
}
