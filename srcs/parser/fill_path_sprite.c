/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fill_path_sprite.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cbertran <cbertran@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/23 19:13:44 by cbertran          #+#    #+#             */
/*   Updated: 2020/06/07 18:40:00 by cbertran         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

static int	get_id_map(t_env_cub3d *var, char *s, int x)
{
	var->parse.y = 0;
	if (!var->parse.id[0])
		while (var->parse.y < 34)
			var->parse.id[var->parse.y++] = '0';
	while (s[x] == ' ')
		x++;
	var->parse.y = 0;
	var->parse.id[var->parse.y] = '2';
	return (0);
}

static int	fill_path_sprite_else(char *s, int x)
{
	if (s[x + 1] == '\0' || (s[x] == 'S' && s[x + 1] != ' '))
	{
		if (s[x] == 'S' && s[x + 1] == 'K' && s[x + 2] == 'Y' && s[x + 3] == 'B'
			&& s[x + 4] == 'O' && s[x + 5] == 'X')
			return (1);
		else
			ft_error("Incorrect path for texture", 6);
	}
	return (0);
}

static void	check_parse(t_env_cub3d *env)
{
	env->parse.type_sprite = 0;
	env->parse.check.sprite++;
	if (env->parse.check.sprite > 1)
		ft_error("Sprite already exist", 6);
}

static void	while_parse(t_env_cub3d *env, int y, int *is_sprite)
{
	env->parse.texture.sprite[env->parse.id_map][y] = '\0';
	(!*is_sprite) ? (*is_sprite = env->parse.texture.texture_check++) : 0;
	env->parse.texture.type_sprite[env->parse.id_map] = env->parse.type_sprite;
}

void		fill_path_sprite(t_env_cub3d *env, char *s, t_texture *var, int x)
{
	static int	is_sprite;
	int			y;
	char		save[BUFFER_SIZE];

	check_parse(env);
	while (s[x])
	{
		if (s[x] == '.')
		{
			y = 0;
			save[y++] = s[x++];
			while (s[x] != ' ' && s[x])
				save[y++] = s[x++];
			save[y] = '\0';
			env->parse.id_map = get_id_map(env, s, x);
			ft_memcpy(var->sprite[env->parse.id_map], save, y);
			while_parse(env, y, &is_sprite);
			(env->parse.type_sprite != 1) ? (check_texture(save)) : 0;
			break ;
		}
		if (fill_path_sprite_else(s, x++))
			return ;
	}
	check_after_argument(s, x);
}
