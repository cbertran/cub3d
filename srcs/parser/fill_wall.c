/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fill_wall.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cbertran <cbertran@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/04/09 18:23:05 by cbertran          #+#    #+#             */
/*   Updated: 2020/05/29 00:32:23 by cbertran         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

int			get_wall(t_env_cub3d *var, char c)
{
	static int	boolean;
	int			z;

	if (!boolean)
	{
		var->wall[0].type = '0';
		var->wall[0].height = 0;
		boolean = 1;
	}
	z = 0;
	while (z < 28 && var->wall[z].type != '#')
	{
		if (var->wall[z].type == c)
			return (z);
		z++;
	}
	return (z);
}

static void	color_while(char *save, int i, t_env_cub3d *var)
{
	double		nbr;

	nbr = ft_atof(save);
	nbr < 0 ? ft_error("Wall height under 0", 18) : 0;
	var->wall[i].height = nbr;
}

void		fill_height_wall(char *s, t_env_cub3d *var)
{
	int		x;
	int		y;
	int		i;
	char	save[11];

	x = 1;
	while (s[x])
	{
		if ((s[x] != ' ') && (s[x] == '1' || (s[x] >= 'a' && s[x] <= 'z')))
		{
			i = get_wall(var, s[x]);
			if ((s[x + 1] != ' '))
				ft_error("Incorrect ID for wall height definition", 18);
			var->wall[i].type = s[x++];
			y = 0;
			while (s[x] == ' ')
				x++;
			while (y < 11 && ((s[x] >= '0' && s[x] <= '9') || s[x] == '.'))
				save[y++] = s[x++];
			save[y] = '\0';
			color_while(save, i, var);
			break ;
		}
		x++;
	}
}
