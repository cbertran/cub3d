/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fill_path.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cbertran <cbertran@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/16 00:55:49 by cbertran          #+#    #+#             */
/*   Updated: 2020/06/07 18:07:54 by cbertran         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

/*
** Type: 1 : NO | 2 : SO | 3 : WE | 4 : EA | 5 : S
*/

static void	fill_path_string(char *save, t_texture *var, int type, int y)
{
	if (type == 1)
	{
		ft_memcpy(var->north, save, y);
		var->north[y] = '\0';
	}
	else if (type == 2)
	{
		ft_memcpy(var->south, save, y);
		var->south[y] = '\0';
	}
	else if (type == 3)
	{
		ft_memcpy(var->west, save, y);
		var->west[y] = '\0';
	}
	else if (type == 4)
	{
		ft_memcpy(var->east, save, y);
		var->east[y] = '\0';
	}
}

static void	check_parse(t_check_parse *parse, int type, char c)
{
	if (c != ' ')
		ft_error("Incorrect path for texture", 6);
	if (type == 1)
		parse->north_texture++;
	else if (type == 2)
		parse->south_texture++;
	else if (type == 3)
		parse->west_texture++;
	else if (type == 4)
		parse->east_texture++;
	if (parse->north_texture > 1 || parse->south_texture > 1
		|| parse->east_texture > 1 || parse->west_texture > 1)
		ft_error("One of the textures already exist", 6);
}

void		check_texture(char *file)
{
	int	fd;

	fd = 0;
	(file) ? fd = open(file, O_RDONLY) : 0;
	if (fd <= 0)
		ft_error("One of the textures does not exist", 10);
	close(fd);
}

void		fill_path(t_env_cub3d *parse, char *s, t_texture *var, int type)
{
	int		x;
	int		y;
	char	save[BUFFER_SIZE];

	x = ((type == 5) ? 1 : 2);
	check_parse(&parse->parse.check, type, s[x]);
	while (s[x])
	{
		if (s[x] == '.')
		{
			y = 0;
			save[y++] = s[x++];
			while (s[x] != ' ' && s[x])
				save[y++] = s[x++];
			save[y] = '\0';
			fill_path_string(save, var, type, y);
			var->texture_check++;
			check_texture(save);
			break ;
		}
		else if (s[x + 1] == '\0' || s[x] != ' ')
			ft_error("Incorrect path for texture", 6);
		x++;
	}
	check_after_argument(s, x);
}
