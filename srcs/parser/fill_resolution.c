/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fill_resolution.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cbertran <cbertran@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/15 23:11:00 by cbertran          #+#    #+#             */
/*   Updated: 2020/06/07 16:53:45 by cbertran         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

static void	fst(t_env_cub3d *env, int x, t_resolution *resolution, int state)
{
	if (x < 0)
		ft_error("Incorrect resolution", 5);
	if (state == 0)
		resolution->x = x;
	else if (state == 1)
	{
		resolution->y = x;
		env->parse.check.resolution++;
	}
}

static void	check_after_resolution(t_env_cub3d *env, const char *s)
{
	if (env->parse.check.resolution > 1)
		ft_error("Resolution is already set", 5);
	while (*s)
	{
		if (*s != ' ')
			ft_error("Forbidden character", 20);
		s++;
	}
}

void		fill_resolution(t_env_cub3d *env, const char *s, t_resolution *var,
								int y)
{
	static int	state = 0;
	int			x;
	char		save[12];

	var->check = 1;
	s++;
	while (*s)
	{
		(*s && *s == '-') ? ft_error("Incorrect resolution", 5) : 0;
		if (*s && *s != ' ' && (*s >= '0' && *s <= '9'))
		{
			y = 0;
			while (*s >= '0' && *s <= '9')
				save[y++] = *s++;
			save[y] = '\0';
			x = ft_atoi(save);
			fst(env, x, var, state++);
		}
		else if (*s == ' ' || (*s >= '0' && *s <= '9'))
			s++;
		else
			ft_error("Forbidden character", 20);
	}
	check_after_resolution(env, s);
	state != 2 ? ft_error("Incorrect resolution", 5) : 0;
}
