/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_link.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cbertran <cbertran@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/21 18:17:39 by cbertran          #+#    #+#             */
/*   Updated: 2020/06/05 03:41:57 by cbertran         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

static int	ft_strlen(char *line)
{
	size_t	x;

	x = 0;
	while (*line)
	{
		x++;
		line++;
	}
	return (x);
}

static int	check_save_parse(t_env_cub3d *var, char *line, char *sch)
{
	int	x;

	x = 0;
	while (line[x] != '\0')
	{
		if ((line[x] == ' ' && line[x + 1] == sch[0])
			|| (x == 0 && line[x] == sch[x]))
		{
			if (ft_strstr(line, sch) && ft_strlen(line) == 6)
			{
				var->parse.count_arg++;
				return (1);
			}
		}
		else if (line[x] != ' ')
			break ;
		x++;
	}
	return (0);
}

static void	check_argument(t_env_cub3d *env, int argc, char **argv)
{
	if (argc < 1)
		ft_error("No argument", 1);
	else if (argc > 0 && argc < 2)
		ft_error("Empty argument", 1);
	else if (argc > 3)
		ft_error("Too many arguments", 1);
	else if (argc == 3 && !check_save_parse(env, argv[2], "--save"))
		ft_error("Incorrect argument for save image", 1);
	else if (argc == 3)
		env->is_bmp = TRUE;
	else
		env->is_bmp = FALSE;
}

void		check_link(t_env_cub3d *env, int argc, char **argv)
{
	char	*line;
	int		fd;

	line = NULL;
	fd = 0;
	check_argument(env, argc, argv);
	fd = open(argv[1], O_RDONLY);
	if (fd < 0)
		ft_error("The file does not exist", 9);
	if ((line = (ft_strrchr(argv[1], '.'))) != NULL)
	{
		if (ft_strncmp(line, ".cub", 4) != 0)
			ft_error("Wrong file extension, please use .cub", 1);
	}
	else
		ft_error("Wrong file extension, please use .cub", 1);
	close(fd);
}
