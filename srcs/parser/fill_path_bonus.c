/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fill_path_bonus.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cbertran <cbertran@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/16 00:55:49 by cbertran          #+#    #+#             */
/*   Updated: 2020/06/05 01:26:22 by cbertran         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

int			is_different(char *s, char search)
{
	int	diff;

	diff = 0;
	while (*s)
	{
		if (*s != search)
			diff++;
		s++;
	}
	if (*s == '\0' && diff == 0)
		return (0);
	return (1);
}

int			is_present(char s, char *search)
{
	while (*search)
	{
		if (s == *search)
			return (1);
		search++;
	}
	return (0);
}

static void	fill_path_string(char *save, t_texture *var, int type, int y)
{
	if (type == 2)
	{
		ft_memcpy(var->floor, save, y);
		var->floor[y] = '\0';
	}
	else if (type == 1)
	{
		ft_memcpy(var->ceilling, save, y);
		var->ceilling[y] = '\0';
	}
}

void		fill_path_bonus(char *s, t_color *color, t_texture *var, int type)
{
	int		x;
	int		y;
	char	save[BUFFER_SIZE];

	x = 1;
	while (s[x])
	{
		if (s[x] == '.')
		{
			y = 0;
			save[y++] = s[x++];
			while (s[x] != ' ' && s[x])
				save[y++] = s[x++];
			save[y] = '\0';
			fill_path_string(save, var, type, y);
			color->check = 1;
			check_texture(save);
			break ;
		}
		else if (s[x + 1] == '\0' || s[x] != ' ')
			ft_error("Incorrect path for texture", 6);
		x++;
	}
}

void		fill_path_bonus_other(char *s, char *var, int x)
{
	int		y;
	char	save[BUFFER_SIZE];

	while (s[x])
	{
		if (s[x] == '.')
		{
			y = 0;
			save[y++] = s[x++];
			while (s[x] != ' ' && s[x])
				save[y++] = s[x++];
			save[y] = '\0';
			ft_memcpy(var, save, y);
			var[y] = '\0';
			check_texture(save);
			break ;
		}
		else if (s[x + 1] == '\0' || s[x] != ' ')
			ft_error("Incorrect path for texture", 6);
		x++;
	}
}
