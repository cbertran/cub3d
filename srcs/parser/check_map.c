/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_map.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cbertran <cbertran@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/03 18:48:20 by cbertran          #+#    #+#             */
/*   Updated: 2020/06/02 18:51:28 by cbertran         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

static void	check_map_third(t_env_cub3d *var, char *line, int i, int *last_line)
{
	if (line[i] == '1' || line[i] == ' ' || line[i] == '0' || line[i] == 'N'
		|| line[i] == 'S' || line[i] == 'E' || line[i] == 'W' || line[i] == '2')
	{
		(*last_line == 1) ? ft_error("Incorrect map", 3) : 0;
		var->width_map.check_width++;
	}
	else
		ft_error("Incorrect map", 3);
}

static void	check_map_two(t_env_cub3d *var, char *line)
{
	int			i;
	int			boolean;
	static int	last_line;

	i = 0;
	boolean = 0;
	(!last_line) ? (last_line = 0) : 0;
	while (line[i] != '\0')
	{
		if (line[i] == '1' && boolean == 0)
		{
			var->width_map.line++;
			boolean++;
		}
		else
			check_map_third(var, line, i, &last_line);
		i++;
	}
	last_line = (var->width_map.check_width != 0) ? 0 : 1;
	if (var->width_map.width < var->width_map.check_width)
		var->width_map.width = var->width_map.check_width;
}

void		check_map(t_env_cub3d *var, char *line)
{
	int		i;

	i = 0;
	if (var->width_map.boolean == 0)
	{
		var->width_map.line_number_map_boolean = 1;
		while (line && (line[i] == '1' || line[i] == ' '))
			(line[i++] == '1') ? var->width_map.width++ : 0;
		var->width_map.line++;
		var->width_map.boolean = 1;
		var->width_map.width_buf = var->width_map.width;
	}
	else
	{
		var->width_map.check_width = 0;
		check_map_two(var, line);
	}
}

void		if_map_close(t_env_cub3d *var)
{
	int	x;
	int	y;

	x = 0;
	while (x < var->width_map.line)
	{
		y = 0;
		while (y < var->width_map.width)
		{
			if (var->map[x][y] == '0')
				is_escape(var, x, y);
			else if (var->map[x][y] == ' ')
				is_correct_hole(var, x, y);
			else if (ck_le(var->map[x][y]) == 2)
				fill_sprite_struct(var, x, y);
			y++;
		}
		x++;
	}
	if (!var->width_map.player_is_closing)
		ft_error("Ambiguous player placement", 8);
	else
		if_player_escape(var);
}
