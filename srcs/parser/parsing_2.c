/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parsing_2.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cbertran <cbertran@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/06/05 01:44:43 by cbertran          #+#    #+#             */
/*   Updated: 2020/06/07 14:42:01 by cbertran         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

void	sum_check(t_check_parse *var)
{
	var->total = 0;
	if (var->resolution)
		var->total++;
	if (var->north_texture)
		var->total++;
	if (var->south_texture)
		var->total++;
	if (var->east_texture)
		var->total++;
	if (var->west_texture)
		var->total++;
	if (var->sprite)
		var->total++;
	if (var->floor)
		var->total++;
	if (var->ceilling)
		var->total++;
}

void	check_after_argument(char *line, int i)
{
	while (line[i] != '\0')
	{
		if (line[i] != ' ')
			ft_error("Forbidden character", 20);
		i++;
	}
}

void	check_after_ok_sum(char *line)
{
	int i;

	i = 0;
	while (line[i] != '\0')
	{
		if (line[i] == '1' || line[i] == ' ')
			i++;
		else
			ft_error("Forbidden character", 20);
	}
}
