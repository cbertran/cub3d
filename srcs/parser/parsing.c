/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parsing.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cbertran <cbertran@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/15 18:28:32 by cbertran          #+#    #+#             */
/*   Updated: 2020/06/07 16:42:13 by cbertran         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

static void	detection_parsing(t_env_cub3d *var, char *line)
{
	if (check_start_parse(var, line, "R "))
		fill_resolution(var, line, &var->parse.resolution, 0);
	else if (check_start_parse(var, line, "NO"))
		fill_path(var, ft_strstr(line, "NO"), &var->parse.texture, 1);
	else if (check_start_parse(var, line, "SO"))
		fill_path(var, ft_strstr(line, "SO"), &var->parse.texture, 2);
	else if (check_start_parse(var, line, "WE"))
		fill_path(var, ft_strstr(line, "WE"), &var->parse.texture, 3);
	else if (check_start_parse(var, line, "EA"))
		fill_path(var, ft_strstr(line, "EA"), &var->parse.texture, 4);
	else if (check_start_parse(var, line, "S"))
		fill_path_sprite(var, ft_strstr(line, "S"), &var->parse.texture, 0);
	else if (check_start_parse(var, line, "F "))
		is_color(var, line, 2);
	else if (check_start_parse(var, line, "C "))
		is_color(var, line, 1);
	else if (var->parse.check.total <= 8)
	{
		if (var->parse.check.total == 8)
			check_after_ok_sum(line);
		else if (is_different(line, ' '))
			ft_error("Forbidden character", 20);
	}
}

static int	while_gnl_2(t_env_cub3d *var, char *line)
{
	int	i;

	i = 0;
	while (line[i] != '\0' && var->parse.count_arg >= 8)
	{
		if (line[i] == '1')
		{
			check_map(var, line);
			free(line);
			if (var->parse.texture.texture_check < 5)
				ft_error("Missing texture definition", 11);
			else if (var->parse.texture.texture_check > 5)
				ft_error("Duplication of textures", 11);
			return (1);
		}
		if (line[i] != ' ')
			break ;
		i++;
	}
	free(line);
	return (0);
}

static int	while_gnl(t_env_cub3d *var, char *line, int wyn)
{
	if (!var->width_map.line_number_map_boolean)
		var->width_map.line_number_map++;
	if (wyn == 1 && line[0] != '#')
	{
		sum_check(&var->parse.check);
		detection_parsing(var, line);
		if (while_gnl_2(var, line))
			return (1);
	}
	else if (wyn == 2)
	{
		check_map(var, line);
		free(line);
	}
	else if (line[0] == '#')
		free(line);
	return (0);
}

/*
** First pass check map
*/

void		initialize_parse(t_env_cub3d *var, char *file)
{
	int		fd;
	int		ret;
	char	*line;

	fd = 0;
	ret = 0;
	line = NULL;
	reset_fill_parse(var);
	(file) ? fd = open(file, O_RDONLY) : 0;
	while ((ret = get_next_line(fd, &line)) > 0)
		if (while_gnl(var, line, 1))
			break ;
	while ((ret = get_next_line(fd, &line)) > 0)
		(line[0] != '#') ? while_gnl(var, line, 2) : free(line);
	(ret == 0) ? check_map(var, line) : 0;
	(ret <= 0) ? free(line) : 0;
	close(fd);
	var->width_map.line_number_map--;
	if (var->parse.resolution.check != 1)
		ft_error("Missing resolution definition", 12);
	if (var->parse.floor.check != 1)
		ft_error("Missing floor color definition", 13);
	if (var->parse.ceilling.check != 1)
		ft_error("Missing ceilling color definition", 14);
	fill_map(var, file);
}
