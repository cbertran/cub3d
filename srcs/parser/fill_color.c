/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fill_color.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cbertran <cbertran@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/15 23:10:32 by cbertran          #+#    #+#             */
/*   Updated: 2020/06/07 17:45:35 by cbertran         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

static void	fill_struct_color(int x, t_color *color, int state)
{
	if (state == 0)
		color->r = x;
	else if (state == 1)
		color->g = x;
	else if (state == 2)
	{
		color->b = x;
		color->check = 1;
		color->hexa = calc_color(color);
	}
}

static int	color_while(char *save, char c, t_color *var)
{
	static int	state;
	int			nbr;

	if (!(state) || state == 3)
		state = 0;
	nbr = ft_atoi(save);
	nbr < 0 ? ft_error("Color under 0", 7) : 0;
	nbr > 255 ? ft_error("Color upper 255", 7) : 0;
	fill_struct_color(nbr, var, state++);
	if (c != ',')
		return (-1);
	return (0);
}

void		fill_color(char *s, t_color *var, int x, int y)
{
	int		nbr_color;
	char	save[5];

	nbr_color = 0;
	var->hexa = 0;
	while (s[x])
	{
		if ((s[x] != ' ') && ((s[x] >= '0' && s[x] <= '9') || s[x] == '-'))
		{
			y = 0;
			nbr_color++;
			save[y++] = s[x++];
			while (y < 3 && s[x] >= '0' && s[x] <= '9')
				save[y++] = s[x++];
			y > 3 ? ft_error("Color upper 255", 7) : (save[y] = '\0');
			if (color_while(save, s[x], var) == -1)
				break ;
		}
		else if (is_present(s[x], " ,-0123456789"))
			x++;
		else
			ft_error("Forbidden character", 20);
	}
	check_after_argument(s, x);
	nbr_color != 3 ? ft_error("Wrong color", 7) : 0;
}

/*
** 1 : Ceilling | 2: Floor
*/

void		is_color(t_env_cub3d *env, char *s, int type)
{
	if (type == 1 && env->parse.texture.ceilling[0] != '.')
	{
		env->parse.check.floor++;
		if (env->parse.check.floor > 1)
			ft_error("Floor color already exist", 7);
		fill_color(s, &env->parse.ceilling, 1, 0);
	}
	else if (type == 2 && env->parse.texture.floor[0] != '.')
	{
		env->parse.check.ceilling++;
		if (env->parse.check.ceilling > 1)
			ft_error("Ceilling color already exist", 7);
		fill_color(s, &env->parse.floor, 1, 0);
	}
}
