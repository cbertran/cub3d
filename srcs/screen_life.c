/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   screen_life.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cbertran <cbertran@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/09 18:49:18 by cbertran          #+#    #+#             */
/*   Updated: 2020/05/28 23:50:26 by cbertran         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

static void			print_number(t_env_cub3d *env, t_screen_number *var,
										t_file_tex *tex, int wyn)
{
	t_number	number;

	if (wyn == 100)
	{
		number.x = var->width * 12;
		number.y = (env->parse.resolution.y - (env->parse.resolution.y / 8));
		number.x_end = number.x + var->width;
		number.y_end = env->parse.resolution.y;
		number.center_x = number.x + var->width / 2;
		number.center_y = number.y + var->height / 2;
	}
	else if (wyn == 10)
	{
		number.x = var->width * 13;
		number.y = (env->parse.resolution.y - (env->parse.resolution.y / 8));
		number.x_end = number.x + var->width;
		number.y_end = env->parse.resolution.y;
		number.center_x = number.x + var->width / 2;
		number.center_y = number.y + var->height / 2;
	}
	draw_number(env, var, &number, tex);
}

static void			unit(t_env_cub3d *env, t_screen_number *var,
							t_file_tex *tex)
{
	t_number	number;

	number.x = var->width * 14;
	number.y = (env->parse.resolution.y - (env->parse.resolution.y / 8));
	number.x_end = number.x + var->width;
	number.y_end = env->parse.resolution.y;
	number.center_x = number.x + var->width / 2;
	number.center_y = number.y + var->height / 2;
	draw_number(env, var, &number, tex);
}

static t_file_tex	*choose_number(t_env_cub3d *env, int x)
{
	if (x == 1)
		return (&env->player.number.one);
	else if (x == 2)
		return (&env->player.number.two);
	else if (x == 3)
		return (&env->player.number.three);
	else if (x == 4)
		return (&env->player.number.four);
	else if (x == 5)
		return (&env->player.number.five);
	else if (x == 6)
		return (&env->player.number.six);
	else if (x == 7)
		return (&env->player.number.seven);
	else if (x == 8)
		return (&env->player.number.eight);
	else if (x == 9)
		return (&env->player.number.nine);
	return (&env->player.number.zero);
}

static void			health_text(t_env_cub3d *env)
{
	t_number		number;
	t_screen_number	var;

	var.width = env->parse.resolution.x / 6;
	var.height = env->parse.resolution.y / 22;
	number.x = var.width * 4;
	number.y = (env->parse.resolution.y - (env->parse.resolution.y / 6));
	number.x_end = number.x + var.width;
	number.y_end = number.y + var.height;
	number.center_x = number.x + ((number.x_end - number.x) / 2);
	number.center_y = number.y + ((number.y_end - number.y) / 2);
	draw_number(env, &var, &number, &env->player.number.health);
}

void				screen_life(t_env_cub3d *env)
{
	t_screen_number	var;
	int				num;

	var.height = env->parse.resolution.y / 8;
	var.width = env->parse.resolution.x / 18;
	num = (int)(env->player.life.life / 100);
	print_number(env, &var, choose_number(env, num), 100);
	num = (int)(env->player.life.life / 10);
	(num > 10) ? (num = num % 10) : 0;
	print_number(env, &var, choose_number(env, num), 10);
	num = (int)(env->player.life.life);
	(num > 10) ? (num = num % 10) : 0;
	unit(env, &var, choose_number(env, num));
	health_text(env);
}
