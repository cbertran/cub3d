/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   raycasting_2.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cbertran <cbertran@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/10 17:18:35 by cbertran          #+#    #+#             */
/*   Updated: 2020/05/24 19:49:15 by cbertran         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

void		found_max_height(t_env_cub3d *env, double *max_height)
{
	int	z;

	z = 0;
	*max_height = 0;
	while (z < 28 && env->wall[z].type != '#')
	{
		if (env->wall[z].height > *max_height)
			*max_height = env->wall[z].height;
		z++;
	}
}

double		calc_wall(t_env_cub3d *env, double last_height)
{
	double			wall_height;
	double			ratio;
	double			difference;

	wall_height = env->wall[get_wall(env, env->ray.map_case)].height;
	ratio = env->player.plane.distance_plane / env->ray.distance;
	env->ray.wall.height = (wall_height / env->ray.distance)
							* env->player.plane.distance_plane;
	env->ray.wall.end = (int)(ratio * env->player.plane.z_position
							+ env->player.plane.head_position);
	env->ray.wall.remove_last = (last_height / env->ray.distance)
							* env->player.plane.distance_plane;
	env->ray.wall.end -= env->ray.wall.remove_last;
	difference = wall_height - last_height;
	(difference <= 0.0) ? (difference = 0.0) : 0;
	env->ray.wall.start = (int)((env->ray.wall.end
		- (env->player.plane.distance_plane * difference / env->ray.distance)));
	(env->ray.wall.end > env->screen_height) ?
		(env->ray.wall.end = env->screen_height) : 0;
	return (wall_height);
}

/*
** Step : Wall before touch max_height, things, end of ray
*/

static void	if_detect_sprite(t_env_cub3d *env)
{
	int	i;
	int	z;

	i = 0;
	while (i < env->nbr_sprite)
	{
		z = 0;
		if ((int)env->sprite[i].x == env->ray.map_x
			&& (int)env->sprite[i].y == env->ray.map_y)
		{
			while (env->parse.id[z] != '0')
			{
				if (env->sprite[i].type == env->parse.id[z])
				{
					env->sprite[i].if_see = 1;
					env->sprite[i].height[env->ray.column] = env->ray.i;
					if (env->ray.column == (int)(env->player.plane.center_x)
						&& env->player.weapon.id_sprite_shoot == -1)
						env->player.weapon.id_sprite_shoot = env->sprite[i].id;
				}
				z++;
			}
		}
		i++;
	}
}

static void	if_detect_2(t_env_cub3d *env, int wyn)
{
	if (wyn == 1)
	{
		calc_distance_with_wall(env);
		env->ray.distance_for_texture = env->ray.distance;
		env->ray.distance *= cos(env->ray.angle_fisheye * DEG_TO_RAD);
	}
	else if (wyn == 2)
	{
		draw_wall(env, env->current_height, TRUE);
		env->ray.hit = 1;
		env->current_height = (env->player.height - 0.5);
	}
}

void		if_detect(t_env_cub3d *env)
{
	static double	max_height;

	(!max_height) ? (found_max_height(env, &max_height)) : 0;
	if (!is_present(env->ray.map_case, "23456789ABCDEFGHIJKLMNOPQRSTUVWXYZ")
	&& (env->current_height
		!= env->wall[get_wall(env, env->ray.map_case)].height))
	{
		if_detect_2(env, 1);
		draw_wall(env, env->current_height, FALSE);
		env->current_height = env->wall[get_wall(env,
											env->ray.map_case)].height;
	}
	if ((env->ray.map_x <= 0 || env->ray.map_x >= env->width_map.line - 1)
		|| (env->ray.map_y <= 0 || env->ray.map_y >= env->width_map.width - 1))
		if_detect_2(env, 2);
	else
		if_detect_sprite(env);
}
