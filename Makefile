# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: cbertran <cbertran@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2019/10/09 14:41:55 by cbertran          #+#    #+#              #
#    Updated: 2020/06/05 01:59:50 by cbertran         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME		:= 	cub3d
SRCS		:=	main.c \
				\
				functions/gnl/get_next_line.c functions/gnl/get_next_line_utils.c \
				functions/gnl/get_next_line_utils_2.c functions/ft_strstr.c \
				functions/ft_atoi.c functions/ft_memcpy.c functions/ft_strjoin.c \
				functions/ft_putnbr.c functions/ft_putstr.c \
				functions/ft_putchar.c functions/ft_strncmp.c functions/ft_strrchr.c \
				functions/ft_error.c functions/ft_itoa_base.c functions/ft_sort.c \
				functions/ft_atof.c functions/ft_rand.c functions/ft_is_prime.c \
				\
				srcs/parser/fill_resolution.c srcs/parser/fill_color.c \
				srcs/parser/fill_path.c srcs/parser/fill_map.c srcs/parser/check_map.c \
				srcs/parser/if_escape.c srcs/parser/check_sprite.c srcs/parser/is_hole.c \
				srcs/parser/check_link.c srcs/parser/malloc.c srcs/parser/calc_color.c \
				srcs/parser/fill_path_bonus.c srcs/parser/parsing.c srcs/parser/parsing_2.c \
				srcs/parser/if_player_escape.c srcs/parser/fill_path_sprite.c \
				srcs/parser/fill_wall.c srcs/parser/bonus_sprite.c srcs/parser/initialize.c \
				\
				srcs/mymlx/pixel_put.c srcs/mymlx/drawing_rectangle.c srcs/mymlx/drawing_circle.c \
				srcs/mymlx/drawing_line.c srcs/mymlx/generate_image.c srcs/mymlx/get_color.c \
				\
				srcs/draw_hud.c srcs/mlx_event.c srcs/mlx_event2.c srcs/ft_sort_distance.c \
				srcs/screen_number.c srcs/move_player.c srcs/player.c srcs/screenshot.c \
				srcs/initialize_draw.c srcs/texturing.c srcs/free.c srcs/raycasting.c \
				srcs/raycasting_2.c srcs/raycasting_3.c srcs/sprite.c srcs/sprite_2.c \
				srcs/sprite_3.c srcs/floor_casting.c srcs/special_movement.c srcs/hand.c \
				srcs/weapon.c srcs/initialize_draw_2.c srcs/initialize_number.c \
				srcs/screen_life.c srcs/initialize_sprite.c
OBJS		:=	$(SRCS:.c=.o)
SRCS_BONUS	:=	bonus/main.c \
				\
				bonus/functions/gnl/get_next_line.c bonus/functions/gnl/get_next_line_utils.c \
				bonus/functions/gnl/get_next_line_utils_2.c bonus/functions/ft_strstr.c \
				bonus/functions/ft_atoi.c bonus/functions/ft_memcpy.c bonus/functions/ft_strjoin.c \
				bonus/functions/ft_putnbr.c bonus/functions/ft_putstr.c \
				bonus/functions/ft_putchar.c bonus/functions/ft_strncmp.c bonus/functions/ft_strrchr.c \
				bonus/functions/ft_error.c bonus/functions/ft_itoa_base.c bonus/functions/ft_sort.c \
				bonus/functions/ft_atof.c bonus/functions/ft_rand.c bonus/functions/ft_is_prime.c \
				\
				bonus/srcs/parser/fill_resolution.c bonus/srcs/parser/fill_color.c \
				bonus/srcs/parser/fill_path.c bonus/srcs/parser/fill_map.c \
				bonus/srcs/parser/check_map.c bonus/srcs/parser/if_escape.c \
				bonus/srcs/parser/check_sprite.c bonus/srcs/parser/is_hole.c \
				bonus/srcs/parser/check_link.c bonus/srcs/parser/malloc.c \
				bonus/srcs/parser/calc_color.c bonus/srcs/parser/fill_path_bonus.c \
				bonus/srcs/parser/parsing.c bonus/srcs/parser/if_player_escape.c \
				bonus/srcs/parser/fill_path_sprite.c bonus/srcs/parser/fill_wall.c \
				bonus/srcs/parser/bonus_sprite.c bonus/srcs/parser/initialize.c \
				bonus/srcs/parser/parsing_2.c \
				\
				bonus/srcs/mymlx/pixel_put.c bonus/srcs/mymlx/drawing_rectangle.c \
				bonus/srcs/mymlx/drawing_circle.c bonus/srcs/mymlx/drawing_line.c \
				bonus/srcs/mymlx/generate_image.c bonus/srcs/mymlx/get_color.c \
				\
				bonus/srcs/draw_hud.c bonus/srcs/mlx_event.c bonus/srcs/mlx_event2.c \
				bonus/srcs/ft_sort_distance.c bonus/srcs/screen_number.c \
				bonus/srcs/move_player.c bonus/srcs/player.c bonus/srcs/screenshot.c \
				bonus/srcs/initialize_draw.c bonus/srcs/texturing.c bonus/srcs/free.c \
				bonus/srcs/raycasting.c bonus/srcs/raycasting_2.c bonus/srcs/raycasting_3.c \
				bonus/srcs/sprite.c bonus/srcs/sprite_2.c bonus/srcs/sprite_3.c \
				bonus/srcs/floor_casting.c bonus/srcs/special_movement.c bonus/srcs/hand.c \
				bonus/srcs/weapon.c bonus/srcs/sound.c bonus/srcs/initialize_draw_2.c \
				bonus/srcs/initialize_number.c bonus/srcs/screen_life.c bonus/srcs/initialize_sprite.c
OBJS_BONUS	:=	$(SRCS_BONUS:.c=.o)
RM			:=	rm -f
CFLAGS		?=  -Wall -Wextra -Werror -g
UNAME		:= $(shell uname)
ifeq ($(UNAME), Linux)
		LIBRARY		?=	-L functions/mlx_linux
		MLX_LIB		?=	functions/mlx_linux
$(NAME):LINK		?=	-lmlx -lXext -lX11 -lm -lbsd
bonus:	LINK		?=	-lmlx -lXext -lX11 -lm -lbsd -lSDL -lSDL_mixer `sdl-config --cflags --libs`
$(NAME):CPPFLAGS	?=	-I ./includes/linux -I ./includes/both -I ./functions/mlx_linux
bonus:	CPPFLAGS	?=	-I ./bonus/includes/linux -I ./bonus/includes/both -I ./functions/mlx_linux
endif
ifeq ($(UNAME), Darwin)
		LIBRARY		?=	-L functions/mlx
		MLX_LIB		?=	functions/mlx
$(NAME):LINK		?=	-lmlx -framework OpenGL -framework AppKit
bonus:	LINK		?=	-lmlx -framework OpenGL -framework AppKit `sdl-config --cflags --libs`
$(NAME):CPPFLAGS	?=	-I ./includes/darwin -I ./includes/both -I ./functions/mlx_linux
bonus:	CPPFLAGS	?=	-I ./bonus/includes/darwin -I ./bonus/includes/both -I ./functions/mlx
endif

$(NAME): $(OBJS)
	-@$(MAKE) MAKEFLAGS= -C $(MLX_LIB) all 2>/dev/null
	$(info Compilation in progress, please wait)
	-@$(CC) $(LIBRARY) $(CPPFLAGS) -o $(NAME) $(OBJS) $(LINK)

all: $(NAME)

bonus : $(OBJS_BONUS)
	-@$(MAKE) MAKEFLAGS= -C $(MLX_LIB) all 2>/dev/null
	$(info Compilation in progress, please wait)
	-@$(CC) $(LIBRARY) $(CPPFLAGS) -o $(NAME) $(OBJS_BONUS) $(LINK)

fclean:	clean
	-@$(RM) $(NAME)

clean:
	$(info Clean in progress, please wait)
	-@$(MAKE) MAKEFLAGS= -C $(MLX_LIB) clean 2>/dev/null
	-@$(RM) $(OBJS)
	-@$(RM) $(OBJS_BONUS)

re:	fclean
	$(MAKE) all
