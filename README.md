# Cub3D

Cub3d is a project of 42 recreating wolfenstein 3d

### Note

:star: Outstanding project

:heavy_check_mark: Base 6/6

:heavy_check_mark: Bonus 5/5

:heavy_check_mark: Bonus+ 5/5

---

## Installation

```bash
git clone https://gitlab.com/cbertran/cub3d.git
```

### No bonus
```bash
make -j
```
### With bonus
```bash
sudo apt update
sudo sh sh/sdl_install.sh
make bonus -j
```
List of bonus :
 - [x] Commentary in cub file
 - [x] Wall collisions
 - [x] A skybox
 - [x] An HUD
 - [x] A Minimap
 - [x] Speed
 - [x] Ability to look up and down
 - [x] Jump or crouch
 - [x] Life bar
 - [x] More items in the maze
 - [x] Object collisions
 - [x] Earning points and/or losing life by picking up objects/traps
 - [x] Animations of a gun shot
 - [x] Several levels
 - [x] Sounds and music
 - [x] Weapons and bad guys to fight
 - [x] Animated Sprite
 - [x] Miscellaneous enemy addition system
 - [x] Take screenshoot anytime with F12
 - [x] Different wall and floor heights


## Usage

1. Base level, no bonus
```bash
./cub3d level/00/map.cub
```
![level_0](https://gitlab.com/cbertran/cub3d/-/raw/master/pictures/1.jpg)

2. Bonus level 1
```bash
./cub3d level/01/map.cub
```
![level_1](https://gitlab.com/cbertran/cub3d/-/raw/master/pictures/2.jpg)

3. Bonus level 2
```bash
./cub3d level/02/map.cub
```
![level_2](https://gitlab.com/cbertran/cub3d/-/raw/master/pictures/3.jpg)
