#!bin/sh

EXEC=cub3d
ARGS=level/00/map.cub
SAVE=--save
valgrind --tool=memcheck --leak-check=full --leak-resolution=high --show-reachable=yes --log-file=valgrin.log ./$EXEC $ARGS #$SAVE
grep -A1 "valgrind" valgrin.log | grep $EXEC
