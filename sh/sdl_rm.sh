#!bin/bash

if ! which libsdl2-2.0-0 > /dev/null;
then
    apt remove -y libsdl2-2.0-0 libsdl2-dev libsdl-mixer1.2 libsdl-mixer1.2-dev
    apt autoremove -y
fi
