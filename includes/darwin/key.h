/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   key.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cbertran <cbertran@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/02 17:48:45 by cbertran          #+#    #+#             */
/*   Updated: 2020/03/23 18:40:28 by cbertran         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef KEY_H
# define KEY_H

# define UP 13
# define DOWN 1
# define LEFT 0
# define RIGHT 2
# define TURN_LEFT 123
# define TURN_RIGHT 124
# define SHIFT_LEFT 257
# define SHIFT_RIGHT 258
# define LOOK_UP 126
# define LOOK_DOWN 125
# define CROUCH 8
# define SCREENSHOT 66
# define JUMP 49
# define QUIT 53
# define SPEED 1
# define FOV 60.0

#endif
