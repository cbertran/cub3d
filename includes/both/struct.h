/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   struct.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cbertran <cbertran@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/15 15:39:24 by cbertran          #+#    #+#             */
/*   Updated: 2020/06/04 19:32:19 by cbertran         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_STRUCT_H
# define FT_STRUCT_H
# define BUFFER_SIZE 4096
# define MAX_SPRITE 10000
# include "cub3d.h"
# include "my_mlx.h"
# include "player.h"

typedef struct		s_texture
{
	int				texture_check;
	char			north[BUFFER_SIZE];
	char			south[BUFFER_SIZE];
	char			west[BUFFER_SIZE];
	char			east[BUFFER_SIZE];
	char			sprite[35][BUFFER_SIZE];
	int				type_sprite[35];
	char			floor[BUFFER_SIZE];
	char			ceilling[BUFFER_SIZE];
	char			skybox[BUFFER_SIZE];
	char			hand[BUFFER_SIZE];
}					t_texture;

typedef struct		s_color
{
	int				r;
	int				g;
	int				b;
	int				a;
	int				hexa;
	int				check;
}					t_color;

typedef struct		s_resolution
{
	int				x;
	int				y;
	int				check;
}					t_resolution;

typedef struct		s_id_map
{
	int				x;
	int				type;
	int				number;
}					t_id_map;

typedef struct		s_check_parse
{
	int				resolution;
	int				north_texture;
	int				south_texture;
	int				east_texture;
	int				west_texture;
	int				sprite;
	int				floor;
	int				ceilling;
	int				total;
}					t_check_parse;

typedef struct		s_parse
{
	t_resolution	resolution;
	t_texture		texture;
	t_color			floor;
	t_color			ceilling;
	t_color			hud;
	t_color			minimap_block;
	t_color			minimap_back;
	t_color			minimap_player;
	t_check_parse	check;
	int				type_map;
	int				count_arg;
	int				if_wall_def;
	int				is_hud;
	int				id_map;
	int				type_sprite;
	int				value_sprite;
	int				y;
	int				type[35];
	int				number[35];
	char			id[35];
}					t_parse;

typedef struct		s_width_map
{
	int				line;
	int				line_buf;
	int				line_number_map_boolean;
	int				line_number_map;
	int				x;
	int				is_player;
	int				player_x;
	int				player_y;
	int				player_is_closing;
	int				width;
	int				width_buf;
	int				check_width;
	int				see_place;
	int				boolean;
}					t_width_map;

typedef	struct		s_wall
{
	double			height;
	int				start;
	int				end;
	int				remove_last;
	int				height_floor_case;
	int				end_floor_case;
}					t_wall;

typedef struct		s_sprite
{
	t_file_tex		*link;
	int				channel;
	double			x;
	double			y;
	double			z;
	double			if_see;
	double			distance;
	double			angle;
	double			x_move;
	double			y_move;
	double			futur_height;
	int				*height;
	int				center_x;
	int				size;
	int				start_x;
	int				end_x;
	int				start_y;
	int				end_y;
	int				id;
	int				tab_adress;
	int				is_shoot;
	int				shooting;
	int				is_shooting;
	int				sprite_type;
	int				health;
	int				frame;
	int				x_case;
	int				y_case;
	char			type;
	char			map_case;
}					t_sprite;

typedef struct		s_sort_sprite
{
	t_sprite		*save_tab;
	int				count_one;
	int				count_two;
	int				i;
	int				i_save;
}					t_sort_sprite;

typedef	struct		s_ray_t
{
	t_wall			wall;
	double			side_dist_x;
	double			side_dist_y;
	double			delta_dist_x;
	double			delta_dist_y;
	double			distance;
	double			distance_for_texture;
	double			angle_fisheye;
	double			wall_hit_x;
	float			orig_x;
	float			orig_y;
	float			dir_x;
	float			dir_y;
	float			end_x;
	float			end_y;
	int				map_x;
	int				map_y;
	int				step_x;
	int				step_y;
	int				hit;
	int				side;
	int				column;
	int				found_sprite;
	int				i;
	int				limit;
	char			map_case;
}					t_ray_t;

typedef struct		s_hand
{
	int				size;
	int				start_x;
	int				start_y;
	int				end_x;
	int				end_y;
}					t_hand;

typedef	struct		s_wall_type
{
	char			type;
	double			height;
}					t_wall_type;

typedef	struct		s_screen_number
{
	int				height;
	int				width;
	int				center_x;
	int				center_y;
	char			*number;
}					t_screen_number;

typedef struct		s_number
{
	int				x;
	int				x_end;
	int				y;
	int				y_end;
	int				center_x;
	int				center_y;
	int				d;
	int				col;
}					t_number;

typedef struct		s_env_cub3d
{
	t_width_map		width_map;
	t_sprite		sprite[MAX_SPRITE];
	t_wall_type		wall[32];
	t_parse			parse;
	t_image			game;
	t_image			hit;
	t_texture_img	texture;
	t_player		player;
	t_ray_t			ray;
	t_window		window;
	t_hand			hand;
	double			screen_height;
	double			current_height;
	int				sort_sprite[MAX_SPRITE];
	int				nbr_sprite;
	int				current_sprite;
	int				line;
	int				column;
	int				is_bmp;
	char			*argv;
	char			**map;
	char			is_hit[63];
}					t_env_cub3d;

#endif
