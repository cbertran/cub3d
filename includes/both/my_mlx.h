/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   my_mlx.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cbertran <cbertran@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/23 15:53:07 by cbertran          #+#    #+#             */
/*   Updated: 2020/05/24 02:39:23 by cbertran         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_MY_MLX_H
# define FT_MY_MLX_H
# define BUFFER_SIZE 4096

typedef struct	s_window
{
	void		*mlx;
	void		*window;
}				t_window;

typedef struct	s_grid
{
	int			x;
	int			y;
}				t_grid;

typedef	struct	s_image
{
	int			*address;
	int			bpp;
	int			line_length;
	int			end;
	void		*img;
}				t_image;

typedef struct	s_rectangle
{
	int			width;
	int			height;
	int			x;
	int			y;
	int			border;
	int			color;
}				t_rectangle;

typedef struct	s_circle
{
	int			radius;
	int			x;
	int			x_center;
	int			y;
	int			y_center;
	int			border;
	int			color;
}				t_circle;

typedef struct	s_line
{
	int			border;
	int			x1;
	int			y1;
	int			x2;
	int			y2;
	int			color;
}				t_line;

typedef struct	s_file_tex
{
	double		wall_hit_x;
	double		step;
	double		tex_pos;
	int			width;
	int			height;
	int			tex_x;
	int			tex_y;
	int			*address;
	int			bpp;
	int			line_length;
	int			end;
	void		*img;
}				t_file_tex;

typedef struct	s_bmp
{
	short		bpp;
	short		color_scheme;
	int			adress;
	int			size;
	int			header_size;
	int			raw;
	int			res;
	char		header[54];
}				t_bmp;

typedef struct	s_spr_tex
{
	t_file_tex	death_1;
	t_file_tex	death_2;
	t_file_tex	death_3;
	t_file_tex	death_4;
	t_file_tex	death_5;
	t_file_tex	fire_1;
	t_file_tex	fire_2;
	t_file_tex	shoot;
	t_file_tex	walk_1;
	t_file_tex	walk_2;
	t_file_tex	walk_3;
	t_file_tex	walk_4;
	t_file_tex	alone;
	int			type;
	int			value;

}				t_spr_tex;

typedef	struct	s_texture_img
{
	t_file_tex	north;
	t_file_tex	south;
	t_file_tex	east;
	t_file_tex	west;
	t_spr_tex	sprite[35];
	t_file_tex	floor;
	t_file_tex	ceilling;
	t_file_tex	skybox;
}				t_texture_img;

#endif
