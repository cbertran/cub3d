/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   function.h                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cbertran <cbertran@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/15 15:54:37 by cbertran          #+#    #+#             */
/*   Updated: 2020/05/29 00:36:39 by cbertran         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_FUNCTION_H
# define FT_FUNCTION_H
# include <stdlib.h>
# include <unistd.h>

typedef struct	s_sort
{
	int			*save_tab;
	int			count_one;
	int			count_two;
	int			i;
	int			i_save;
}				t_sort;

char			*ft_strstr(char *str, char *to_find);
void			*ft_memcpy(void *dst, const void *src, size_t n);
int				ft_strncmp(const char *s1, const char *s2, size_t n);
char			*ft_itoa_base(int number, int base, int upper);
char			*ft_strrchr(const char *s, int c);
char			*ft_strjoin_2(char const *s1, char const *s2);
int				ft_atoi(const char *str);
char			*ft_itoa(int n);
char			*ft_strdup(const char *s1);
void			ft_putstr(const char *s);
void			ft_putchar(const char c);
void			ft_sort(int *tab, int length);
double			ft_atof(char *s);
void			ft_putnbr(int x);
int				ft_is_prime(int nb);
int				ft_rand(unsigned int seed);
int				ft_rrand(unsigned int seed, int limit);

#endif
